@isTest
private class AddLee_TriggerEmailMessageHandlerTest {
	@testSetup 
    static void setupMethod() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = false);
     	insert disabletrigger;
        NoValidations__c noValidation = new NoValidations__c(SetupOwnerId = UserInfo.getUserID(), Active_Users__c = true);
     	insert noValidation;
	}
    
    static testMethod void NegativeCaseWithComment() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        insert casesToInsert;
        
        string htmlBody = 'Job: #252137 2016-04-09 03:21';
        htmlBody += 'Payment Type: Cash';
        htmlBody += 'Name: Vinnie Padmanabhan';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07568572889';
        htmlBody += 'Pick Up: 2-6 Atlantic Road, London, SW9 8HY';
        htmlBody += 'Drop Off: Highbury & Islington Station, Highbury Corner, London, N5 1RA';
        htmlBody += 'Driver Rating: &#9733;';
        htmlBody += 'Overall Rating: &#9733;';
        htmlBody += 'Comments: \n';
        htmlBody += 'This is awful. Never using this again - not only did it cost be far more than uber would have but I wasn\'t able to redeem the £10 free credit for first time use. Never again.: <br/>';
        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Positive feedback received, job 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        Case caseToVerify = [Select Id, Status, Type, Subject, Driver_Rating__c, External_Id__c, Overall_Rating__c, Send_Feedback_Comment_Emial__c, FeedBackEmail__c, Job_Number__c From Case Limit 1];
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
        system.assertEquals(caseToVerify.Driver_Rating__c, 1.0);
        system.assertEquals(caseToVerify.Overall_Rating__c, 1.0);
        system.assertEquals(caseToVerify.Send_Feedback_Comment_Emial__c, false);
        system.assertEquals(caseToVerify.FeedBackEmail__c, 'dusan.zivkovic1@me.com');
        system.assertEquals(caseToVerify.Job_Number__c, '1114512');
        
    }
    
    static testMethod void NegativeCaseWithNoComment() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        List<Contact> con = AddLee_Trigger_Test_Utils.createContacts(1);
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId,SuppliedEmail='test@test.com', ContactId=con[0].Id ));
        insert casesToInsert;
        
        string htmlBody = 'Job: #368180 2016-04-13 16:30';

        htmlBody += 'Name: Seona Bell';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07856025631';
        htmlBody += 'Pick Up: Andrew Duffus Ltd, Unit 4/A, 2-4 Orsman Road, London, N1 5QJ';
        htmlBody += 'Drop Off: Bell Staff Clothing, 11-15 Emerald Street, London, WC1N 3QL';
        htmlBody += 'Driver Rating: &#9733;&#9733; ';
        htmlBody += 'Overall Rating: &#9733;&#9733; ';
        htmlBody += 'Comments:  </strong> <br/>\n';
        htmlBody += '                     <br/>';

        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Positive feedback received, job 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        Case caseToVerify = [Select Id, Status, Type, Subject, Driver_Rating__c, External_Id__c, Overall_Rating__c, Send_Feedback_Comment_Emial__c, FeedBackEmail__c, Job_Number__c From Case];
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
        System.debug('MTDebug caseToVerify.Driver_Rating__c : ' + caseToVerify.Driver_Rating__c);
        system.assertEquals(2.0, caseToVerify.Driver_Rating__c);
        system.assertEquals(2.0, caseToVerify.Overall_Rating__c);
        system.assertEquals(true, caseToVerify.Send_Feedback_Comment_Emial__c);
        system.assertEquals('Driver', caseToVerify.Type);
        system.assertEquals(caseToVerify.FeedBackEmail__c, 'dusan.zivkovic1@me.com');
        system.assertEquals(caseToVerify.Job_Number__c, '1114512');
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
        System.debug('MTDebug caseToVerify.Driver_Rating__c : ' + caseToVerify.Driver_Rating__c);
    }
    
    static testMethod void PositiveCaseWithComment() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        insert casesToInsert;
        
        string htmlBody = 'Job: #252137 2016-04-09 03:21';
        htmlBody += 'Payment Type: Cash';
        htmlBody += 'Name: Vinnie Padmanabhan';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07568572889';
        htmlBody += 'Pick Up: 2-6 Atlantic Road, London, SW9 8HY';
        htmlBody += 'Drop Off: Highbury & Islington Station, Highbury Corner, London, N5 1RA';
        htmlBody += 'Driver Rating: &#9733;&#9733;&#9733;&#9733;&#9733;';
        htmlBody += 'Overall Rating: &#9733;&#9733;&#9733;&#9733;&#9733;';
        htmlBody += 'Comments:\n';
        htmlBody += 'This is awful. Never using this again - not only did it cost be far more than uber would have but I wasn\'t able to redeem the £10 free credit for first time use. Never again.: <br/>';
        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Positive feedback received, job 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        Case caseToVerify = [Select Id, Status, Type, Subject, Driver_Rating__c, External_Id__c, Overall_Rating__c, Send_Feedback_Comment_Emial__c, FeedBackEmail__c, Job_Number__c From Case Limit 1];
        system.assertEquals(5.0, caseToVerify.Driver_Rating__c);
        system.assertEquals(5.0, caseToVerify.Overall_Rating__c);
        system.assertEquals(false, caseToVerify.Send_Feedback_Comment_Emial__c);
        system.assertEquals(caseToVerify.FeedBackEmail__c, 'dusan.zivkovic1@me.com');
        system.assertEquals(caseToVerify.Job_Number__c, '1114512');
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
    }
    
    static testMethod void PositiveCaseWithNoComment() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        List<Contact> con = AddLee_Trigger_Test_Utils.createContacts(1);
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId,SuppliedEmail='test@test.com', ContactId=con[0].Id ));
        insert casesToInsert;
        
        string htmlBody = 'Job: #368180 2016-04-13 16:30';

        htmlBody += 'Name: Seona Bell';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07856025631';
        htmlBody += 'Pick Up: Andrew Duffus Ltd, Unit 4/A, 2-4 Orsman Road, London, N1 5QJ';
        htmlBody += 'Drop Off: Bell Staff Clothing, 11-15 Eemerald Street, London, WC1N 3QL';
        htmlBody += 'Driver Rating: &#9733;&#9733;&#9733;&#9733;&#9733;';
        htmlBody += 'Overall Rating: &#9733;&#9733;&#9733;&#9733;&#9733;';
        htmlBody += 'Comments:  </strong> <br/>\n';
        htmlBody += '                     <br/>';

        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Positive feedback received, job 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        Case caseToVerify = [Select Id, Status, Type, Subject, Driver_Rating__c, External_Id__c, Overall_Rating__c, Send_Feedback_Comment_Emial__c, FeedBackEmail__c, Job_Number__c From Case];
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
        System.debug('MTDebug caseToVerify.Driver_Rating__c : ' + caseToVerify.Driver_Rating__c);
        system.assertEquals(5.0, caseToVerify.Driver_Rating__c);
        system.assertEquals(5.0, caseToVerify.Overall_Rating__c);
        system.assertEquals(false, caseToVerify.Send_Feedback_Comment_Emial__c);
        system.assertEquals('Driver', caseToVerify.Type);
        system.assertEquals(caseToVerify.FeedBackEmail__c, 'dusan.zivkovic1@me.com');
        system.assertEquals(caseToVerify.Job_Number__c, '1114512');
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
        System.debug('MTDebug caseToVerify.Driver_Rating__c : ' + caseToVerify.Driver_Rating__c);
    }
    
    static testMethod void PositiveCaseWithComment1() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        insert casesToInsert;
        
        string htmlBody = 'Job: #252137 2016-04-09 03:21';
        htmlBody += 'Payment Type: Cash';
        htmlBody += 'Name: Vinnie Padmanabhan';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07568572889';
        htmlBody += 'Pick Up: 2-6 Atlantic Road, London, SW9 8HY';
        htmlBody += 'Drop Off: Highbury & Islington Station, Highbury Corner, London, N5 1RA';
        htmlBody += 'Driver Rating: &#9733;&#9733;&#9733;';
        htmlBody += 'Overall Rating: &#9733;&#9733;&#9733;&#9733;';
        htmlBody += 'Comments:\n';
        htmlBody += 'This is awful. Never using this again - not only did it cost be far more than uber would have but I wasn\'t able to redeem the £10 free credit for first time use. Never again.: <br/>';
        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Positive feedback received, job 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        Case caseToVerify = [Select Id, Status, Type, Subject, Driver_Rating__c, External_Id__c, Overall_Rating__c, Send_Feedback_Comment_Emial__c, FeedBackEmail__c, Job_Number__c From Case Limit 1];
        system.assertEquals(3.0, caseToVerify.Driver_Rating__c);
        system.assertEquals(4.0, caseToVerify.Overall_Rating__c);
        system.assertEquals(false, caseToVerify.Send_Feedback_Comment_Emial__c);
        system.assertEquals(caseToVerify.FeedBackEmail__c, 'dusan.zivkovic1@me.com');
        system.assertEquals(caseToVerify.Job_Number__c, '1114512');
        system.assertEquals('Closed', caseToVerify.Status);
        System.debug('MTDebug caseToVerify : ' + caseToVerify);
    }
}