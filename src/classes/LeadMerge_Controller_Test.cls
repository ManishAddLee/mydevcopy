@isTest
private class LeadMerge_Controller_Test {
	
	@isTest(seeallData = false) 
	static void pageLoadWithNoDuplicates() {
		// Implement test code

		AddLee_Trigger_Test_Utils.insertCustomSettings();

		// Create a test lead 1
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.CompanyDunsNumber = '229515499';
		testLead.PostalCode = 'SE1 0HS';
		insert testLead;

		// Load Lead Merge page without any duplicate leads.
		PageReference pageRef = Page.LeadMerge;
		pageRef.getParameters().put('id',testLead.Id);

		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);

		LeadMerge_Controller theController = new LeadMerge_Controller(controller);
	}

	@isTest(seeallData = false) 
	static void pageLoadWithNoId() {
		// Implement test code

		AddLee_Trigger_Test_Utils.insertCustomSettings();

		// Create a test lead 1
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.CompanyDunsNumber = '229515499';
		testLead.PostalCode = 'SE1 0HS';
		insert testLead;

		// Load Lead Merge page without any duplicate leads.
		PageReference pageRef = Page.LeadMerge;

		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);

		LeadMerge_Controller theController = new LeadMerge_Controller(controller);
	}


	@isTest(seeallData = false) 
	static void pageLoadWithInvalidIds() {
		// Implement test code

		AddLee_Trigger_Test_Utils.insertCustomSettings();

		// Create a test lead 1
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.CompanyDunsNumber = '229515499';
		testLead.PostalCode = 'SE1 0HS';
		insert testLead;

		// Load Lead Merge page without any duplicate leads.
		PageReference pageRef = Page.LeadMerge;
		pageRef.getParameters().put('id','006'+testLead.Id);
		pageRef.getParameters().put('did',testLead.Id);

		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);

		LeadMerge_Controller theController = new LeadMerge_Controller(controller);
	}

	@isTest(seeallData = false)  
	static void pageLoadWithDuplicatesAndMergeLeads() {
		// Implement test code

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Lead> listOfTestLeads = AddLee_Trigger_Test_Utils.createLeads(2);
		// Create a test lead 1
		
		// Lead 1
		listOfTestLeads[0].Bank_Date_of_DD__c = System.today();
		listOfTestLeads[0].CompanyDunsNumber = '229515499';
		listOfTestLeads[0].PostalCode = 'SE1 0HS';
		
		// Lead 2
		listOfTestLeads[1].Bank_Date_of_DD__c = System.today();
		listOfTestLeads[1].CompanyDunsNumber = '229515499';
		listOfTestLeads[1].PostalCode = 'SE1 0HS';
		
		insert listOfTestLeads;

		// EmailTemplate emailTemplate = [SELECT Id, Name, HtmlValue FROM EmailTemplate Where DeveloperName = 'Leads_Merge_Notification_To_Lead_Owner' limit 1];
		// Create a test email template
		//AddLee_Trigger_Test_Utils.insertEmailTemplate('Leads_Merge_Notification_To_Lead_Owner');
		
		// Load Lead Merge page without any duplicate leads.
		PageReference pageRef = Page.LeadMerge;
		pageRef.getParameters().put('id',listOfTestLeads[0].Id);
		pageRef.getParameters().put('did', listOfTestLeads[1].Id);
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(listOfTestLeads[0]);

		LeadMerge_Controller theController = new LeadMerge_Controller(controller);
		theController.isApexTestRunning = true;
		theController.mergeLeads();
	}


	@isTest(seeallData = false)  
	static void pageLoadWithDuplicatesAndContinueWithExisting() {
		// Implement test code

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Lead> listOfTestLeads = AddLee_Trigger_Test_Utils.createLeads(2);
		// Create a test lead 1
		
		// Lead 1
		listOfTestLeads[0].Bank_Date_of_DD__c = System.today();
		listOfTestLeads[0].CompanyDunsNumber = '229515499';
		listOfTestLeads[0].PostalCode = 'SE1 0HS';
		
		// Lead 2
		listOfTestLeads[1].Bank_Date_of_DD__c = System.today();
		listOfTestLeads[1].CompanyDunsNumber = '229515499';
		listOfTestLeads[1].PostalCode = 'SE1 0HS';
		
		insert listOfTestLeads;

		// EmailTemplate emailTemplate = [SELECT Id, Name, HtmlValue FROM EmailTemplate Where DeveloperName = 'Leads_Merge_Notification_To_Lead_Owner' limit 1];
		// Create a test email template
		//AddLee_Trigger_Test_Utils.insertEmailTemplate('Leads_Merge_Notification_To_Lead_Owner');
		
		// Load Lead Merge page without any duplicate leads.
		PageReference pageRef = Page.LeadMerge;
		pageRef.getParameters().put('id',listOfTestLeads[0].Id);
		pageRef.getParameters().put('did', listOfTestLeads[1].Id);
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(listOfTestLeads[0]);

		LeadMerge_Controller theController = new LeadMerge_Controller(controller);
		theController.isApexTestRunning = true;
		theController.continueWithExisting();
	}


	
}