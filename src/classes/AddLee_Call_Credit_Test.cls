@isTest
private class AddLee_Call_Credit_Test {
    
    @isTest 
    static void AddLee_Call_Credit_Search07a() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        AddLee_Call_Credit.callcreditheaders_element credentials = new AddLee_Call_Credit.callcreditheaders_element();
        credentials.company = 'Addison CR CTEST';
        credentials.username = 'Addison CR API CTEST';
        credentials.password = 'test';
        AddLee_Call_Credit.CT_SearchDefinition SearchDefinition =  new AddLee_Call_Credit.CT_SearchDefinition();
        searchDefinition.yourreference = 'Test';
        searchDefinition.creditrequest = new AddLee_Call_Credit.CT_searchrequest();
        searchDefinition.creditrequest.score = '1';
        searchDefinition.creditrequest.purpose = 'CA';
        searchDefinition.creditrequest.autosearch = '1';
        searchDefinition.creditrequest.autosearchmaximum = '3';
        searchDefinition.creditrequest.schemaversion = '7.1';
        searchDefinition.creditrequest.datasets = 255;
        searchDefinition.creditrequest.applicant = new List<AddLee_Call_Credit.CT_searchapplicant>();
        AddLee_Call_Credit.CT_searchapplicant searchApplicant = new AddLee_Call_Credit.CT_searchapplicant();
        //searchApplicant.dob = thisLead.Birthdate__c;
        searchApplicant.address = new List<AddLee_Call_Credit.CT_inputaddress>();
        AddLee_Call_Credit.CT_inputaddress inputAddress = new AddLee_Call_Credit.CT_inputaddress();
        inputAddress.buildingno = '1';
        inputAddress.postcode = 'test';
        searchApplicant.address.add(inputAddress);
        searchApplicant.name = new List<AddLee_Call_Credit.CT_inputname>();
        AddLee_Call_Credit.CT_inputname inputName = new AddLee_Call_Credit.CT_inputname();
        inputName.forename = 'thisLead.firstName';
        inputName.surname = 'thisLead.lastName';
        searchApplicant.name.add(inputName);
        searchDefinition.creditrequest.applicant.add(searchApplicant);
        System.DEBUG(searchDefinition);
        AddLee_Call_Credit.Soap11 request = new AddLee_Call_Credit.Soap11();
        //request.callcreditheaders = new AddLee_Call_Credit.callcreditheaders_element();
        request.callcreditheaders = credentials;
        System.debug(request);
        AddLee_Call_Credit.CT_SearchResult value ;
        
        Test.StartTest();
       
            Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
            value = request.Search07a(SearchDefinition);
            System.DEBUG(value);
        Test.StopTest();
    }

    @isTest
    static void AddLee_Call_Credit_ChangePassword07a(){
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        AddLee_Call_Credit.Soap11 request = new AddLee_Call_Credit.Soap11();
        Test.StartTest();
            Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
            Boolean response = request.ChangePassword07a('test', 'test');
        Test.StopTest();
        

    }
    
    
}