/*
  *** Marketing Status Batch to poplate Lapsed,No spend,Active accounts (Business and Consumers)  
*/

global class CalculateMarketingStatusOnAccount implements Database.Batchable<sObject>{
   global String Query;
   global String AccountId;

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }
   
       
   global void execute(Database.BatchableContext BC, 
                       List<Account> bscope){
                       
                       List<Account> lstUpdateAccount = new List<Account>();
                       for(Account acc:bscope){
                       if ((acc.Date_of_Last_Booking__c!=null) && (acc.Date_of_Last_Booking__c>=(System.today()-90)) && (acc.Consumer_Account_Number__c != '1') && (acc.Sales_Status__c == 'Lapsed') && (acc.Date_changed_to_Current__c <=(System.today()-180))) {                  
                       acc.Sales_Status__c ='Re-Opened';
                       system.debug('line20++');
                       }
                        system.debug('ispersonaccount----'+acc.IsPersonAccount);
                       //Business have not booked journey last 6 months but never book the journey  
                       if(((acc.Total_Amount_of_Bookings__c==null)||(acc.Total_Amount_of_Bookings__c == 0))&&(acc.Date_changed_to_Current__c<=(System.today()-180)&& (acc.Consumer_Account_Number__c != '1'))){
                       
                       acc.Marketing_Status__c = 'No Spend';
                       system.debug('line22++');
                       }
                       //Consumer have not booked journey last 3 months  
                       else if((acc.Total_Amount_of_Bookings__c!=null) && (acc.Date_of_Last_Booking__c<=(System.today()-90)) && (acc.Consumer_Account_Number__c == '1')){
                        acc.Marketing_Status__c = 'Lapsed';
                        system.debug('consumerlapased');
                        
                       }
                       //Consumer Have not booked single journey 
                       else if(((acc.Total_Amount_of_Bookings__c==null)||(acc.Total_Amount_of_Bookings__c == 0)) && (acc.Consumer_Account_Number__c =='1')){
                         acc.Marketing_Status__c = 'No Spend';
                       }
                       
                       //Business have not booked journey 6 months but have before that
                       if((acc.Date_of_Last_Booking__c!=null) && (acc.Date_of_Last_Booking__c<=(System.today()-180))&& (acc.Consumer_Account_Number__c != '1')){
                       acc.Marketing_Status__c = 'Lapsed';
                       system.debug('line32++');
                       }
                       //Consumer have not booked last 6 months but have before that
                       else if ((acc.Total_Amount_of_Bookings__c !=null)&&(acc.Date_of_Last_Booking__c<=(System.today()-180))&& (acc.Consumer_Account_Number__c == '1')) {
                        acc.Marketing_Status__c = 'Prospects';
                         system.debug('line27++');
                       }
                       //business have booked journey last 6 months
                       if((acc.Date_of_Last_Booking__c>=(System.today()-180))&&(acc.Consumer_Account_Number__c != '1')){
                       acc.Marketing_Status__c = 'Active';
                       system.debug('line43++');
                       }
                       //Consumer have booked journey last 3 months
                       else if((acc.Date_of_Last_Booking__c>=(System.today()-90))&&(acc.Consumer_Account_Number__c == '1')){
                        acc.Marketing_Status__c = 'Active';
                        system.debug('line48++');
                       }           
                       if ((acc.Date_of_Last_Booking__c!=null) && (acc.Date_of_Last_Booking__c <= (system.today()-90))&& (acc.Consumer_Account_Number__c != '1')){
                       acc.Sales_Status__c = 'Lapsed';
                       system.debug('line59++');
                       }                                                                 
                       if((acc.Date_of_Last_Booking__c!=null) && (acc.Date_of_Last_Booking__c>(System.today()-90)) && (acc.Consumer_Account_Number__c != '1') && (acc.Sales_Status__c == 'Lapsed') && (acc.Date_changed_to_Current__c >=(System.today()-180))) {
                       acc.Sales_Status__c = 'Re-Activated';
                       system.debug('line67++');
                       }                                                                        
                       lstUpdateAccount.Add(acc);
                       }
                       
                       Update lstUpdateAccount;
   }

   global void finish(Database.BatchableContext BC){

   
   }

}