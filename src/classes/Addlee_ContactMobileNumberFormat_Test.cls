//@isTest (SeeAllData=true)
@isTest
Public Class Addlee_ContactMobileNumberFormat_Test{
Public static testMethod void Mobilenumber() {
AddLee_TestDataUtilClass addleetduc = new AddLee_TestDataUtilClass();
 addleetduc.Data_InsertLogIntegrationCustomSetting();
 addleetduc.Data_InsertMobileNumberCustomSetting();
 Account acccc = new Account();
   acccc.Name = 'Test Account';
   acccc.Account_Status__c = 'Prospect';
   acccc.Marketing_Status__c = 'Lapsed';
   acccc.Sales_Ledger__c='Sales Ledger';
   acccc.Grading__c ='P2';
   acccc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
  
       
   insert acccc;
    System.assert(acccc != null);

   list<Contact>conn = new list<Contact>();
   Contact conn3 = new Contact(LastName = 'Test Account1',AccountId = acccc.id,Active__c = true,Phone = '989898989',Mobile_Number__c=false,MobilePhone = '7898918989', et4ae5__Mobile_Country_Code__c='ET',Email = 'test@gmail.com');
   Contact conn1 = new Contact(LastName = 'Test Account2',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '07898989189', et4ae5__Mobile_Country_Code__c='GB',Email = 'test@gmail.com');
   Contact conn2 = new Contact(LastName = 'Test Account3',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '444447898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn4 = new Contact(LastName = 'Test Account4',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '+4789819898', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn5 = new Contact(LastName = 'Test Account5',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '4407898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn6 = new Contact(LastName = 'Test Account6',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '444407898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn7 = new Contact(LastName = 'Test Account7',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '44447898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn8 = new Contact(LastName = 'Test Account8',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '447898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn9 = new Contact(LastName = 'Test Account9',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '+447898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn10 = new Contact(LastName = 'Test Account9',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '+447898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');
   Contact conn11 = new Contact(LastName = 'Test Account9',AccountId = acccc.id,Active__c = true,Phone = '989898989', Mobile_Number__c=false,MobilePhone = '+447898198989', et4ae5__Mobile_Country_Code__c='AL',Email = 'test@gmail.com');   
   conn.add(conn1);
   conn.add(conn2);
   conn.add(conn3);
   conn.add(conn4);
   conn.add(conn5);
   conn.add(conn6);
   conn.add(conn7);
   conn.add(conn8);
   conn.add(conn9);
   conn.add(conn10);
   conn.add(conn11);
   string s1 = conn1.MobilePhone;
   string ss = conn1.MobilePhone.deletewhitespace();
   string regExp = '[a-zA-Z;,+.?+/:-_(){}~#|@&*£%$=!]';
   string replacement = '';
   string s2 = s1.replaceAll(regExp,replacement);
   insert conn;
    s2.startswith('7');
    //update conn1.Mobile_Number__c=true;
    //update conn1.et4ae5__Mobile_Country_Code__c='GB';
    Addlee_ContactPhone_Batch cn = new Addlee_ContactPhone_Batch();
    cn.query = 'select Id,firstname,lastname,Phone from Contact where Phone!=null';
    Database.executebatch(cn);
    
    Addlee_ContactMobileNumberFormat_Batch cmn = new Addlee_ContactMobileNumberFormat_Batch();
    cmn.query = 'select Id,accountid,firstname,lastname,MobilePhone,et4ae5__Mobile_Country_Code__c,Mobile_Number__c from Contact where mobilephone!=null';
    Database.executebatch(cmn,20);
    }
   }