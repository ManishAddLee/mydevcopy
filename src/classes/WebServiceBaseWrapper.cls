public abstract class WebServiceBaseWrapper {
   
     
    //The configuration record containing the web service configuration
    //public Apex_Configuration_Info__c ac{get;set;} - Create an object
    
    //The unique name of the configuration record
    public String configRecordName{get;set;}
    
    //The web service name is the complete name of the Apex class that houses the web service method
    public String webServiceName{get;set;}
    
    //The web service method name
    public String webServiceMethodName{get;set;}
    
    //The base64EnCoded authentication key
    public String authenticationKey{get;set;}
    
    /*
    * Default constructor that initializes the service name,
    * and method name as well as based on the unique name of the
    * configuration populates the configuration information.
    * Calls the prepareCommonData() method.
    *
    * @see prepareCommonData
    * @param String crName is the unique name of the corresponding cofig record
    * @param String cName is the name of the service in this apex class
    * @param String mName is the web service method
    */ 
    public WebServiceBaseWrapper(String crName,
        String cName, String mName)
    {
        configRecordName = crName;
        webServiceName= cName;
        webServiceMethodName = mName;
        prepareCommonData();
    }
    
    /*
    * This method returns a map containing basic authentication
    * parameters. A child class can always implements its own
    * authentication header if it requires a different entry.
    * The returned map will contain only one entry and it will be
    * keyed by 'Authorization' and value will be (' Basic '+this.authenticationKey)
    * 
    * @return Map<String,String>
    */
    public Map<String, String> getAuthenticationKeyMap()
    {
        Map<String, String> authMap = new Map<String,String>();         
        //authMap.put('Authorization',' Basic '+this.authenticationKey);
        authMap.put('Accept-Encoding','compress, gzip');
        authMap.put('Content-Encoding','compress, gzip');
        authMap.put('Content-Length', '5000');
        return authMap;
    }
    
    /*
    * This method is used to populate the configuration object
    * based on the unique name of it.
    * It populates the insertLog, maxTries and authenticationKey
    * from the configuration record. 
    * Children or any client should not call this method
    * explicitely as it is already used in defualt constuctor.
    *
    * @see default constructor
    */
    public void prepareCommonData()
    {
        //Query from object once created
        /*
        ac = [select a.User_Name__c, a.Unique_Name__c, a.Password__c,
                    a.End_Point_Url__c, a.Other_Value__c
                    from Apex_Configuration_Info__c a
                    where a.Unique_Name__c= :configRecordName];
        if(!Utility.isEmptyString(ac.user_name__c) && !Utility.isEmptyString(ac.Password__c))
        {
            authenticationKey = EncodingUtil.base64Encode(Blob.valueOf(ac.user_name__c+':'+ac.Password__c)); 
        }
        */
        //Remove Later
        authenticationKey = EncodingUtil.base64Encode(Blob.valueOf('salesforce'+':'+'salesforce'));
    }
    
    /*
    * This method must be implemented by the children classes to prepare security header, actual parameter values etc.
    * Children or any client should not call this method explicitely as it is already used in performWebserviceCall()
    * method as part of template pattern
    *
    * @see performWebserviceCall
    */
    public abstract void prepareData();
    
    /*
    * This method must be implemented by the children classes. This is the most important and it is intended to be the method 
    * actually performs the web service method call to the external service.
    * Children or any client should not call this method explicitely as it is already used in performWebserviceCall()
    * method as part of template pattern.
    *
    * @see performWebserviceCall    
    */
    public abstract void execute();
    
    /*
    * This method must be implemented by the children classes. This method can be used to handles exceptions case by case basis.
    * Children or any client should not call this method explicitely as it is already used in performWebserviceCall()
    * method as part of template pattern.
    *
    * @see performWebserviceCall
    */
    public abstract void handleException(Exception ex);
    
    /*
    * This method implements the template pattern and must be called by the client code.
    * This method prepares data to make web service call via prepareData.
    * This method also calls execute method that calls out to the web service method.
    *
    * @see prepareData()
    * @see execute()
    */
        
    public void performWebserviceCall()
    {
        //system.debug(logginglevel.warn,'1.Start PrepareDate Number of script statements used so far : ' + Limits.getScriptStatements());
        prepareData();   
        //system.debug(logginglevel.warn,'1.End PrepareDate Number of script statements used so far : ' + Limits.getScriptStatements());   
        try{                
            execute();              
        } catch(Exception e) {              
            System.debug('Error - '+e.getMessage());
            handleException(e);         
        }
    }
}