global class MonthlySpend_Scheduler implements Schedulable {
     global void execute(SchedulableContext SC){
        CalculateMonthlyBookingAverageOnAccount csa = new CalculateMonthlyBookingAverageOnAccount();
        csa.Query='select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account WHERE Total_Amount_of_Bookings__c <= 49000 and Date_of_Last_Booking__c !=null and Date_of_Last_Booking__c = LAST_N_DAYS:3';
        Database.executeBatch(csa,30);
     }
     
}