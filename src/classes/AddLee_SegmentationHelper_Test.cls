@isTest
private class AddLee_SegmentationHelper_Test {

    @isTest
    static void Addlee_SegmentationHelper_isEmployeeSegmentSetAccount() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        createTestData();
        
        
        Account testAccount = AddLee_Trigger_Test_Utils.createAccounts(1)[0];
        testAccount.Industry = 'Business Services';
        testAccount.Industry_Sub_Sector__c = 'Management Consulting';
        testAccount.No_of_London_Employees__c = '100-199';
        testAccount.SHM_main_postcode__c = 'SE1 0HS';
        testAccount.Segment__c = 'Large';
        Test.StartTest();
            insert testAccount;
        Test.StopTest();
        
       // testAccount = [Select Segment__c From Account WHERE Id = :testAccount.Id];
        
        System.assertEquals('Large', testAccount.Segment__c);
        // TO DO: implement unit test
    }

    @isTest
    static void Addlee_SegmentationHelper_isEmployeeSegmentSetAccountWithTotalLondEmployees() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        createTestData();
        
        
        Account testAccount = AddLee_Trigger_Test_Utils.createAccounts(1)[0];
        testAccount.Industry = 'Business Services';
        testAccount.Industry_Sub_Sector__c = 'Management Consulting';
        testAccount.Total_London_Employees__c = 150;
        testAccount.SHM_main_postcode__c = 'SE1 0HS';
        testAccount.Segment__c = 'Large';
        Test.StartTest();
            insert testAccount;
        Test.StopTest();
        
       // testAccount = [Select Segment__c From Account WHERE Id = :testAccount.Id];
        
        System.assertEquals('Large', testAccount.Segment__c);
        // TO DO: implement unit test
    }
    
    @isTest
    static void Addlee_SegmentationHelper_isEmployeeSegmentNotSetAccount() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        createTestData();
        
        
        Account testAccount = AddLee_Trigger_Test_Utils.createAccounts(1)[0];
        testAccount.No_of_London_Employees__c = '100-199';
        
        Test.StartTest();
            insert testAccount;
        Test.StopTest();
        
        testAccount = [Select Segment__c From Account WHERE Id = :testAccount.Id];
        
        System.assertEquals(null, testAccount.Segment__c);
        // TO DO: implement unit test
    }
    
    @isTest
    static void Addlee_SegmentationHelper_isEmployeeSegmentSetLead() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        createTestData();
        
        
        Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
        testLead.Industry = 'Business Services';
        testLead.Industry_Sub_Sector__c = 'Management Consulting';
        testLead.No_of_London_Employees__c = '100-199';
        System.DEBUG('testLead : ' + testLead);
        Test.StartTest();
            insert testLead;
        Test.StopTest();
        
        testLead = [Select Segment__c, Inner_Outer__c From Lead WHERE Id = :testLead.Id];
        System.DEBUG('testLead : ' + testLead.Segment__c + ' InnerOuter : ' + testLead.Inner_Outer__c);
        System.assertEquals('Large', testLead.Segment__c);
        // TO DO: implement unit test
    }

    @isTest
    static void Addlee_SegmentationHelper_isEmployeeSegmentSetLeadWithTotalLondEmployees() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        createTestData();
        
        
        Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
        testLead.Industry = 'Business Services';
        testLead.Industry_Sub_Sector__c = 'Management Consulting';
        testLead.Total_London_Employees__c = 150;
        System.DEBUG('testLead : ' + testLead);
        Test.StartTest();
            insert testLead;
        Test.StopTest();
        
        testLead = [Select Segment__c, Inner_Outer__c From Lead WHERE Id = :testLead.Id];
        System.DEBUG('testLead : ' + testLead.Segment__c + ' InnerOuter : ' + testLead.Inner_Outer__c);
        System.assertEquals('Large', testLead.Segment__c);
        // TO DO: implement unit test
    }
    
    @isTest
    static void Addlee_SegmentationHelper_isEmployeeSegmentNotSetLead() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        createTestData();
        
        
        Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
        testLead.No_of_London_Employees__c = '100-199';
        
        Test.StartTest();
            insert testLead;
        Test.StopTest();
        
        testLead = [Select Segment__c From Lead WHERE Id = :testLead.Id];
        
        System.assertEquals(null, testLead.Segment__c);
        // TO DO: implement unit test
    }
    
    @isTest
    static void Addlee_SegmentationHelper_isTargetActiveUsersSetAccount(){
      AddLee_Trigger_Test_Utils.insertCustomSettings();
      createTestData();
    }
    

    @isTest
    static void createTestData(){
      insertTestPostCodeMap();
      insertTestEmployeeSizeBand();
      insertTestTargetActiveUsersBand();
      insertTestExpectedActiveUsersBand();
      insertTestTargetSpendExpenditure();
      insertTestExpectedSpendExpenditure();
    }
    
    @isTest
    static void insertTestPostCodeMap(){
        Post_Code_Lookup__c testPostCodeLookup = new Post_Code_Lookup__c(Inner_Outer__c = 'Inner', PostCode__c = 'SE1');
        insert testPostCodeLookup;
    }

    @isTest
    static void insertTestEmployeeSizeband(){ 
      Swim_Lane__c testEmployeeSizeBand = new Swim_Lane__c(Industry_Type__c = 'Management Consulting', Sole_Trader__c = 'Small', X6_to_10__c = 'Small', X50_to_99__c = 'Medium', X500_to_999__c = 'Large', X20_to_49__c = 'Medium', X200_to_499__c = 'Large', X1_to_5__c = 'Small' , X11_to_19__c = 'Small', X100_to_199__c = 'Large', X1000__c = 'Large' );
      insert testEmployeeSizeBand;
    }
    
    @isTest
    static void insertTestTargetActiveUsersBand(){ 
      Active_Users__c testActiveUsers = new Active_Users__c(Industry_Type__c = 'Management Consulting', Type__c = 'Target Users', Sole_Trader__c = null, X6_to_10__c = 54, X50_to_99__c = 20, X500_to_999__c = 40, X20_to_49__c = 38, X200_to_499__c = 40, X1_to_5__c = 91 , X11_to_19__c = 59, X100_to_199__c = 40, X1000__c = 34 );
      insert testActiveUsers;
    }
    
    @isTest
    static void insertTestExpectedActiveUsersBand(){ 
      Active_Users__c testActiveUsers = new Active_Users__c(Industry_Type__c = 'Management Consulting', Type__c = 'Expected Users', Sole_Trader__c = null, X6_to_10__c = 34, X50_to_99__c = 10, X500_to_999__c = 9, X20_to_49__c = 20, X200_to_499__c = 12, X1_to_5__c = 54 , X11_to_19__c = 27, X100_to_199__c = 15, X1000__c = 8 );
      insert testActiveUsers;
    }
    
    @isTest
    static void insertTestTargetSpendExpenditure(){ 
      Spend_Per_Employee__c testSpendPerEmployee = new Spend_Per_Employee__c(Industry_Type__c = 'Management Consulting', Type__c = 'Target Spend', Sole_Trader__c = null, X6_to_10__c = 724, X50_to_99__c = 274, X500_to_999__c = 783, X20_to_49__c = 496, X200_to_499__c = 848, X1_to_5__c = 1039 , X11_to_19__c = 645, X100_to_199__c = 645, X1000__c = 593 );
      insert testSpendPerEmployee;
    }
    
    @isTest
    static void insertTestExpectedSpendExpenditure(){ 
      Spend_Per_Employee__c testSpendPerEmployee = new Spend_Per_Employee__c(Industry_Type__c = 'Management Consulting', Type__c = 'Expected Spend', Sole_Trader__c = null, X6_to_10__c = 271, X50_to_99__c = 102, X500_to_999__c = 173, X20_to_49__c = 204, X200_to_499__c = 307, X1_to_5__c = 354 , X11_to_19__c = 264, X100_to_199__c = 276, X1000__c = 155 );
      insert testSpendPerEmployee;
    }
}