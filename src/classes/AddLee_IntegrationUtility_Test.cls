/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddLee_IntegrationUtility_Test {

	@isTest
	static void AddLee_IntegrationUtility_Test(){
		Log_GeneralSettings__c generalSettings = new Log_GeneralSettings__c(Name='settings', numCallout__c=10);
		Log_Integration__c integrationSettings = new Log_Integration__c(Name='shamrock_setting', CurrentSessionId__c='12345', Username__c='username', Password__c='password');
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Log__c> logs = AddLee_Trigger_Test_Utils.createLogs(1,'Create');
		Test.StartTest();
			insert generalSettings;	
			insert integrationSettings;
			insert logs;
			AddLee_IntegrationUtility.storeSessionId('12345');
		Test.stopTest();
	
		AddLee_IntegrationUtility.getSessionId();
		AddLee_IntegrationUtility.getPassword();
		AddLee_IntegrationUtility.getUsername();
		AddLee_IntegrationUtility.getMaxCalloutSingleTask();
		AddLee_IntegrationUtility.GetNumFutureCallsInLast24Hours();
		AddLee_IntegrationUtility.getMaxFutureCallsAllowed();
		AddLee_IntegrationUtility.CanUseFutureContext();
		AddLee_IntegrationUtility.getListAccount(logs);
		
		System.AssertEquals('12345',AddLee_IntegrationUtility.getSessionId());
		System.AssertEquals('username',AddLee_IntegrationUtility.getUsername());
		System.AssertEquals('password',AddLee_IntegrationUtility.getPassword());
		
	
	}


	@isTest
	static void AddLee_IntegrationUtility_Test_Exception(){
		Log_GeneralSettings__c generalSettings = new Log_GeneralSettings__c(Name='settings', numCallout__c=10);
		//Log_Integration__c integrationSettings = new Log_Integration__c(Name='shamrock_setting', CurrentSessionId__c=null, Username__c=null, Password__c=null);
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Log__c> logs = AddLee_Trigger_Test_Utils.createLogs(1,'Create');
		System.AssertEquals('test',AddLee_IntegrationUtility.getSessionId());
		System.AssertEquals('test',AddLee_IntegrationUtility.getUsername());
		System.AssertEquals('test',AddLee_IntegrationUtility.getPassword());
		
	
	}



}