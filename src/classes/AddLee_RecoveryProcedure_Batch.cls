global class AddLee_RecoveryProcedure_Batch implements Database.batchable<sObject>,Database.AllowsCallouts {
    
    String query;
    public boolean isApexTest = false;
    global AddLee_RecoveryProcedure_Batch(){
        query  = 'SELECT Account__c, Type__c, Status__c FROM Log__c WHERE isFailed__c = 1 OR isWaiting__c = 1 ORDER BY createdDate ASC';
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
         List<Log__c> failedLogs = new List<Log__c>();
         failedLogs = (List<Log__c>)scope;
         List<Account> accounts = AddLee_IntegrationUtility.getListAccount(failedLogs);
         Map<Account,Log__c> returned = new Map<Account,Log__c>();
         List<Account> accountsToBeupdated = new List<Account>();
         List<Log__c> logsToBeUpdated = new List<Log__c>();
         String sessionId = '';
         if(!isApexTest){
         	sessionId  = AddLee_IntegrationWrapper.loginCalloutSynch();
         }
         System.DEBUG('FailedLogs Batch Result Size : ' + failedLogs.size());
         if(failedLogs.size()>0){
            for(Log__c eachLog : failedLogs){
                if(eachLog.Type__c.equalsIgnoreCase('Create')){
                	//System.DEBUG('FailedLogs Batch Result Account: ' + eachLog.Account__c);
                	//System.DEBUG('Batch Create');
                    returned = AddLee_IntegrationWrapper.createAccountCalloutSynchronous(eachLog.account__c, eachLog.Id,sessionId);
                    //numberOfCallouts++;
                }else if(eachLog.Type__c.equalsIgnoreCase('Update')){
                	//System.DEBUG('FailedLogs Batch Result Account: ' + eachLog.Account__c);
                	//System.DEBUG('Batch Update');
                    returned =AddLee_IntegrationWrapper.updateAccountCalloutSynchronous(eachLog.account__c, eachLog.Id,sessionId);
                    //numberOfCallouts++;
                }  
                accountsToBeUpdated.addAll(returned.keyset());
                logsToBeUpdated.addAll(returned.values());     
            }
            System.Debug('Size Of returned :' + accountsToBeUpdated.size());
            Database.update(accountsToBeUpdated,false);
            Database.update(logsToBeUpdated,false);
        }
    }
   
    global void finish(Database.BatchableContext BC){
    }
}