/* Mobile Number Format For Leads.
   Depends on the mobile number,reformat to 07 */

global class Addlee_LeadMobileNumberFormat_Batch implements Database.Batchable<Sobject>{
    global string query;
    global database.querylocator start(Database.BatchableContext BC){
        return Database.getquerylocator(query);
    }
    global void execute (Database.BatchableContext BC,list<Lead>leads){
        
        Mobile_Number__c num = Mobile_Number__c.getinstance('Number');
        list<Lead>getnos = new list<Lead>();
        for(Lead cc:leads){
            if(cc.MobilePhone!=null){
                string s1 = cc.MobilePhone;
                string ss = cc.MobilePhone.deletewhitespace();
                string regExp = '[a-zA-Z;,+.?+/:-_(){}~#|@&*£%$=!]';
                string replacement = '';
                string s2 = ss.replaceAll(regExp,replacement);
                system.debug('String is  +++' + s2);
                if(s2!= null){       
                    if(s2.startswith('7') && s2.length() == 10){
                        cc.MobilePhone = num.Prefix__c + s2;
                        cc.et4ae5__Mobile_Country_Code__c='GB';
                    }else if(s2.startswith('7') && s2.length() != 10){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.startswith('07') && s2.length() == 11){
                        String s9 = s2.removeStart('0');
                        cc.MobilePhone = num.Prefix__c + s9;
                        cc.et4ae5__Mobile_Country_Code__c='GB';
                    }else if(s2.startswith('07') && s2.length() != 11){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.startswith('444407') && s2.length() == 15){
                        String s4 = s2.removeStart('44440');
                        cc.MobilePhone = num.Prefix__c + s4;
                        cc.et4ae5__Mobile_Country_Code__c='GB';    
                    }else if(s2.startswith('444407') && s2.length() != 15){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    } 
                    else if(s2.startswith('44447') && s2.length() == 14){
                        String s12 = s2.removeStart('4444');
                        cc.MobilePhone = num.Prefix__c + s12;
                        cc.et4ae5__Mobile_Country_Code__c='GB';             
                    }else if(s2.startswith('44447') && s2.length() != 14){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }              
                    else if(s2.startswith('444447') && s2.length() == 15){
                        String s3 = s2.removeStart('44444');
                        cc.MobilePhone = num.Prefix__c + s3;
                        cc.et4ae5__Mobile_Country_Code__c='GB';            
                    }else if(s2.startswith('444447') && s2.length() != 15){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }                            
                    else if(s2.startswith('4407')&& s2.length() == 13){
                        String s5 = s2.removeStart('440');
                        cc.MobilePhone = num.Prefix__c + s5;
                        cc.et4ae5__Mobile_Country_Code__c='GB';             
                    }else if(s2.startswith('4407')&& s2.length() != 13){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.startswith('447')&& s2.length() == 12){
                        String s6 = s2.removeStart('44');
                        cc.MobilePhone = num.Prefix__c + s6;
                        cc.et4ae5__Mobile_Country_Code__c='GB';              
                    }else if(s2.startswith('447')&& s2.length() != 12){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.startswith('+447')&& s2.length() == 12){
                        String s7 = s2.removeStart('+44');
                        cc.MobilePhone = num.Prefix__c + s7;
                        cc.et4ae5__Mobile_Country_Code__c='GB';
                    }else if(s2.startswith('+447')&& s2.length() != 12){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.startswith('+47')&& s2.length() == 11){
                        String s8 = s2.removeStart('+4');
                        cc.MobilePhone = num.Prefix__c + s8;
                        cc.et4ae5__Mobile_Country_Code__c='GB';              
                    }else if(s2.startswith('+47')&& s2.length() != 11){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.startswith('47')&& s2.length() == 11){
                        String s9 = s2.removeStart('4');
                        cc.MobilePhone = num.Prefix__c + s9;
                        cc.et4ae5__Mobile_Country_Code__c='GB';             
                    }else if(s2.startswith('47')&& s2.length() != 11){
                        cc.Mobile_Number_To_Review__c = true;
                        cc.MobilePhone =  s2;
                    }
                    else if(s2.length()<=6){
                        cc.MobilePhone = null;
                    }else if(s2.length()>=7){
                        cc.MobilePhone = s2;
                    }else {}
                    getnos.add(cc); 
                }     
            } 
            
        }
        update getnos;
    }
    global void finish(Database.BatchableContext BC){
    }
}