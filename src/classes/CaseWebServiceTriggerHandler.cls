/*
* Trigger Handler For Adlee Web Service Invocation
*/
public with sharing class CaseWebServiceTriggerHandler {
    public static void processComplaint(List<Case> newCases){
        Refund_Integration__c setting = Refund_Integration__c.getInstance('Shamrock Refund');  
            
        for(Case newCase : newCases){
            //criteria to process
                try{
                    AdleeRefundsServiceInput input = new AdleeRefundsServiceInput();
                    input.adleeRequest = new AdleeRefundsServiceInput.AdleeServiceRequest();
                    input.adleeRequest.createComplain = new AdleeRefundsServiceInput.ComplainRequest();
                    
                    //Complain Data
                    if(setting <> null && setting.DefaultType__c <> null){
                        input.adleeRequest.createComplain.type = setting.DefaultType__c;//Default
                    }
                    //Complain Data
                    if(setting <> null && setting.DefaultTypeId__c <> null){
                        input.adleeRequest.createComplain.typeId = setting.DefaultTypeId__c;//Default
                    }
                    input.adleeRequest.createComplain.subType = newCase.Case_Sub_Type__c;
                    input.adleeRequest.createComplain.account = newCase.AccountId;
                    input.adleeRequest.createComplain.Driver=newcase.Driver_Call_Sign__c;
                    input.adleeRequest.createComplain.Name=newcase.contactId;
                    input.adleeRequest.createComplain.net= newcase.Net_Amount_To_Refund__c;
                    input.adleeRequest.createComplain.gross= newcase.Gross_Amount_To_Refund__c;
                    input.adleeRequest.createComplain.discount= newcase.Discount__c;
                    input.adleeRequest.createComplain.InvoiceNumber= newcase.Invoice_Number__c;
                    input.adleeRequest.createComplain.Note= newcase.Refund_Notes__c;
                    input.adleeRequest.createComplain.Description=newCase.casenumber;
                    input.adleeRequest.createComplain.docketId = newCase.Docket_Id__c;
                    input.adleeRequest.createComplain.docketValue = newCase.Docket_Value__c;
                    input.adleeRequest.createComplain.adminfee = newCase.Admin_Fee__c;
                    input.adleeRequest.createComplain.vat = newCase.Vat__c;
                    
                    input.adleeRequest.parentcaseId  = newCase.id;
                    
                    processCreateComplaint(Json.serialize(input));
                    
                 }catch(Exception e){
                    newCase.id.addError(e.getmessage());
                 }               
       
        }
      }
    
    @future(callout=true)
    public static void processCreateComplaint(String requestjson){
        AdleeRefundsServiceInput input = new AdleeRefundsServiceInput();
        input =  (AdleeRefundsServiceInput)Json.deserialize(requestjson,AdleeRefundsServiceInput.Class);
        //Web service Invokation
        AdleeServiceOutput output = CreateComplainCallout.processAdleeServiceCallout(input);
        
        system.debug('**output*'+output);
        
        if(output.createComplainOutput  <> null){
            String idValue;
            String numberValue;
            if(output.createComplainOutput.id <> null){
                idValue = String.valueof(output.createComplainOutput.id);
            }
            if(output.createComplainOutput.numbervalue <> null){
                numberValue = String.valueof(output.createComplainOutput.numbervalue);
            }
            Utility.processErrorLog(output.createComplainOutput.errorcode,output.createComplainOutput.errorDescription,input.adleeRequest.parentcaseId ,idValue,numberValue);
        }else{
           Utility.processErrorLog('System','Unable to Parse Response',input.adleeRequest.parentcaseId ,'','');
            
        }
       
    }
    

}