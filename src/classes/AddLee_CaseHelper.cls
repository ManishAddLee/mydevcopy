//with sharing
public class AddLee_CaseHelper {
/**
* @author Prateek Gandhi
* @description CaseHelper: Associates entitlement with cases either from the parent account or the generic account
*/ 

    public Static Account account=[Select Id,ParentId,(select id , name from entitlements Where 
                                        Status='Active' Limit 1) from Account where Id =: SLA_Unmanaged__c.getOrgDefaults().X72_Hour_Support__c];
                                        
    Public Static void associateCaseAccountEntitlements(Map<id,List<Case>> accountCasesMap){
        Map<id,List<Case>> accountCasesMap1= new Map<id,List<Case>>();
        Map<Id,Id> mapAccountParentId=new Map<Id,Id>();
        

        List<Account> relatedAccounts=[Select Id, ParentId,Account.Account_Managed__c,(select id , name from entitlements Where 
                                        Status='Active' Limit 1) From Account where Id IN :accountCasesMap.keySet()];// fetching account,active entitlements case
        // TODO: add containsKey Check to maps
        for(Account acc: relatedAccounts){
            if(acc.Entitlements.size()>0){// highest in account in hierarchy with entitlements
                for(Case caseRec: accountCasesMap.get(acc.Id)){
                    caseRec.EntitlementId=acc.Entitlements[0].Id; // Assigning that entitlement to the cases
                    //caseRec.AccountId= AddLee_EmailMessageHelper.isUpdatedInEmailMessageTrigger == true?null: caseRec.AccountId;
                }
            }else if(acc.ParentId != null){
                accountCasesMap1.put(acc.parentId,accountCasesMap.get(acc.Id));    
            }else{
                for(Case caseRec: accountCasesMap.get(acc.Id)){
                    caseRec.EntitlementId=account.Entitlements[0].Id; // Assigning that entitlement to the cases
                    //caseRec.AccountId= AddLee_EmailMessageHelper.isUpdatedInEmailMessageTrigger == true?null: caseRec.AccountId;
                }
            }
        }
        // processing all the case accounts with parent Accounts  
        if(accountCasesMap1.size()>0){
            associateCaseAccountEntitlements(accountCasesMap1);
        }
      
    }

    // Process cases to associate entitlement from related account
    public static void processCases(final List<Case> newCaseList) {
        Map<id,List<Case>> accountIdCaseMap = new Map<id,List<Case>>();

        for(Case caseRecord : newCaseList){
                if(caseRecord.AccountId !=null){ 
                    if(accountIdCaseMap.containsKey(caseRecord.AccountId)){ 
                        accountIdCaseMap.get(caseRecord.AccountId).add(caseRecord);
                    }else{
                        accountIdCaseMap.put(caseRecord.AccountId,new List<Case>{caseRecord});
                    }
                }else{
                    caseRecord.EntitlementId=account.Entitlements[0].Id;
                }
            }
        
        if(accountIdCaseMap.size()>0){
            associateCaseAccountEntitlements(accountIdCaseMap);
        }
    }

    public static void populateMilestoneCompletionDate(Map<Id,Case> casesClosed ){
        DateTime milestoneCompletionDate = System.now();
        List<CaseMilestone> relatedCaseMilestones=[Select Id,CaseId,CompletionDate from CaseMilestone where CompletionDate=null AND CaseId IN : casesClosed.KeySet()];
        if(relatedCaseMilestones.size()>0){
            for(CaseMilestone cmObj : relatedCaseMilestones){
                cmObj.CompletionDate = milestoneCompletionDate;//casesClosed.get(cmObj.CaseId).ClosedDate;
            }
            Update relatedCaseMilestones;
        } 
    }    

}