public without sharing class LeadMerge_Controller {

    // Test Flag
    public Boolean isApexTestRunning {get;set;}

    // Used when there are missing parameters in the list. 
    public List<String> missingParameters {get;set;}
    public String currentLeadId { get; set; } 
    public Set<Id> setOfDuplicateLeadIDs { get;set; }
    public Lead currentLead {get;set;}
    public List<Lead> listOfDuplicateLeads {get;set;}
    public List<FieldSetWrapper> listOfFields {get;set;}
    public String retUrl {get;set;}
    // Return List Of Fields in the fieldset.
    private List<Schema.FieldSetMember> getFields(string accType) {
        if(accType == 'BusinessAccount') return SObjectType.Lead.FieldSets.Lead_Merge.getFields();
        else return SObjectType.Lead.FieldSets.Lead_Merge_Person_Account.getFields();
    }

    public LeadMerge_Controller(ApexPages.StandardController controller) {
        isApexTestRunning = false;
        // Standard Controller. 
        // Get the query string params
        PageReference pageReference = ApexPages.currentPage();
        
        // Add protention against missing parameters
        missingParameters = new List<String>();
        // check if the main lead id is present.
        if(!pageReference.getParameters().containsKey('id')) {
            missingParameters.add('id     - Record Id, there should be a record Id present in the URL parameter');
        }
        
        setOfDuplicateLeadIDs = new Set<Id>();
        // check that there should be atleast 1 duplicate id present.
        for(String eachKey : pageReference.getParameters().keySet()) {
            // did is the keyword for duplicate ids,   
            if(eachKey.startsWithIgnoreCase('did')) {
                // Capture the value of this parameter into a list of ids 
                setOfDuplicateLeadIDs.add(pageReference.getParameters().get(eachKey)); // Automatically checks for invalid ids
            }
        }

        if(setOfDuplicateLeadIDs.size() == 0) {
            missingParameters.add('did[*] - Duplicate Id, there should be atleast 1 duplicate record Id present in the URL parameters');
        }

        // if any missing parameters found, exit the function
        if(missingParameters.size() > 0) {
            return;
        }

        // capture the returnURL
        retUrl = pageReference.getParameters().get('retUrl');

        // Count the number of duplicate lead id's supplied
        currentLeadId = pageReference.getParameters().get('id');
        currentLead = getLeadById(currentLeadId);
        listOfDuplicateLeads = getListOfLeadsbyIds(setOfDuplicateLeadIDs);

        if(currentLead == null) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Lead record not found for the ID :' + currentLeadId);
            ApexPages.addMessage(myMsg);
            return;
        }
        if(listOfDuplicateLeads.size() == 0) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Lead records not found for IDs :' + setOfDuplicateLeadIDs);
            ApexPages.addMessage(myMsg);
            return;
        }

        listOfFields = new List<FieldSetWrapper>();
        for(Schema.FieldSetMember eachField : getFields(pageReference.getParameters().get('accType'))) {
            listOfFields.add(new FieldSetWrapper(eachField, currentLead, listOfDuplicateLeads));
        }

    }

    public PageReference mergeLeads() {
        // Merge Leads, 
        // Iterate throught the collection of leads wrappers and
        Lead updatedLead = new Lead(id=currentLeadId);
        
        for(FieldSetWrapper eachField : listOfFields) {
            // Update the field on this new lead.
            try {
                updatedLead.put(eachField.field.getFieldPath(),eachField.selectedOption);
            }
            catch(Exception e) {
                // Do Nothing, as this error can happen because the field might not be accessible
            }    
        }

        // Update the current lead. 
        update updatedLead;

        // Get all the child relationships of the lead object
        // 
        Map<String, Map<String,String>> mapOfSObjectAndMapOfFieldsAndQuery = new Map<String, Map<String, String>>();
        Schema.DescribeSObjectResult describeResult = Lead.SObjectType.getDescribe();
        List<Schema.ChildRelationship> childRelationships = describeResult.getChildRelationships(); 
        for(Schema.ChildRelationship eachRelationship : childRelationships) {
            // query each child object and 
            // Instead of doing a blind update on all the related obejcts and fields, we should check if the old value on these was one of the duplicate ids. 
            String objectName = String.valueOf(eachRelationship.getChildSObject());
            String relatedField = String.valueOf(eachRelationship.getField());
            String query = 'select id , '+ relatedField +' from '+ objectName +' where '+  relatedField +' IN :setOfDuplicateLeadIDs';
            if(mapOfSObjectAndMapOfFieldsAndQuery.containsKey(objectName)) {
                Map<String, String> mapOfFieldAndQuery = mapOfSObjectAndMapOfFieldsAndQuery.get(objectName);
                mapOfFieldAndQuery.put(relatedField, query);
                mapOfSObjectAndMapOfFieldsAndQuery.put(objectName, mapOfFieldAndQuery);
            } else {
                // Initiate the objectName
                Map<String, String> newMapOfFieldAndQuery = new Map<String, String>();
                newMapOfFieldAndQuery.put(relatedField, query);
                mapOfSObjectAndMapOfFieldsAndQuery.put(objectName, newMapOfFieldAndQuery);
            }
        }



        // Run the quieries for each object and capture the result,
        // If the query results in more than 1 record, then we have to update the recordids for all of these duplicates to the new leadId. 
        // New Structure, Id, Object.Field to update,
        List<FieldsToUpdate> listOfFieldsToUpdate = new List<FieldsToUpdate>();

        // There is no way to query records from salesforce from multiple objects without looping over.  
        for(String eachObject : mapOfSObjectAndMapOfFieldsAndQuery.keySet()) {
            Map<String, String> mapOfFieldAndQuery = mapOfSObjectAndMapOfFieldsAndQuery.get(eachObject);
            for(String eachField : mapOfFieldAndQuery.keySet()) {
                try {
                    //List<sObject> listOfRecords = Database.query(mapOfFieldAndQuery.get(eachField));
                    for(sObject eachRecord : Database.query(mapOfFieldAndQuery.get(eachField))) {
                        // Check If this field was related to the currentLead
                        String fieldValue = String.valueOf(eachRecord.get(eachField));
                        if(setOfDuplicateLeadIDs.contains(fieldValue)) {
                            FieldsToUpdate newRecord = new FieldsToUpdate();
                            newRecord.fieldName = eachField;
                            newRecord.objectName = eachObject;
                            newRecord.recordId = eachRecord.Id;
                            listOfFieldsToUpdate.add(newRecord);
                        } 
                    }
                } catch(Exception ex) {
                    // Do Nothing. this can be caused because of trying to query un-quierable objects
                } 
            }
        }
        
        // Now create a Map for A Update DML Operation;
        Map<Id, sObject> mapOfIdAndSObject = new Map<Id, sObject>();
        
        for(FieldsToUpdate eachField : listOfFieldsToUpdate) {
            String sObjectName = eachField.objectName;
            String fieldName = eachField.fieldName;
            Id recordId = eachField.recordId;

            // Avoid circular dependency
            If(recordId != Id.valueOf(currentLeadId)) {
                sObject sObj = null; 
                if(mapOfIdAndSObject.containsKey(recordId)) {
                    // the object has already been created, just add a field. 
                    sObj = mapOfIdAndSObject.get(recordId);
                } else {
                    sObj = Schema.getGlobalDescribe().get(sObjectName).newSObject();
                }
                sObj.put('Id' , recordId);
                try { 
                    // Set the current lead Id as the new field id.
                    sObj.put(fieldName, currentLeadId); 
                    mapOfIdAndSObject.put(recordId, sObj);
                } Catch (Exception ex) { 
                    // If the field can not be set, then dont update the object. 
                }
            }
        }
        
        // Update all the related sObjects
        update mapOfIdAndSObject.values();
        
        if(!isApexTestRunning) {
            // Send Notification to all the duplicate lead owners.
            sendLeadNotification(currentLead, listOfDuplicateLeads, MessageType.LeadMerged);
        }
        
        // Delete the other leads.
        delete listOfDuplicateLeads;

        if(retUrl != null) {
            // Make sure leadMerge is reattempted for duplicate leads
            retUrl += '&leadMerge=1';
            return new PageReference(retUrl);
        }
        else
            return new PageReference('/'+currentLeadId);
    }

    public PageReference continueWithExisting() {
        // Mark the other leads as duplicate Of
        // TODO : how do we determine that user has clicked on continue with existing lead and wants to go back to the process, without lead merge option. 
        for(Lead eachLead : listOfDuplicateLeads) {
            eachLead.Duplicate_Lead__c = currentLeadId;
        }
        update listOfDuplicateLeads;

        if(!isApexTestRunning) {
         //Send a notification to the duplicate lead owners
         sendLeadNotification(currentLead, listOfDuplicateLeads, MessageType.LeadIgnored);
        }

        if(retUrl != null) {
            retUrl += '&leadMerge=0';
            return new PageReference(retUrl);
        }
       
        else 
            return new PageReference('/'+currentLeadId);
    }

    public PageReference cancelProcess() {
        // Cancel the conversion process and return to the lead.
        // TODO : I have a doubt here that what would happen if this cancel button is pressed after the merge page is loaded from the custom VF tab.
        //        there might be an expectation that, from that page, the cancel button should take the user back to the custom tab. 
        //        But, for now we will keep this as an open option. 
        return new PageReference('/'+currentLeadId);
    }

    public class FieldSetWrapper {
        // The actual field this data belongs to.
        public FieldSetMember field { get;set; }
        // Selected option out of all the available options. 
        public String selectedOption {get;set;}
        // Get Selection Options
        public List<SelectOption> selectionOptions {get;set;}
        // Constructor
        public FieldSetWrapper(FieldSetMember field, Lead originalLead, List<Lead> duplicateLeads){
            String originalLabel = '';
            String originalValue = '';
            Map<String, String> mapOfDuplicateValueAndLabels = new Map<String,String>();
            
            // Initiate the field
            this.field = field;
            // Initiate the selection options. 
            this.selectionOptions = new List<SelectOption>();
            // The Original Lead should always be the first selection option. 
            originalLabel = String.valueOf(originalLead.get(field.getFieldPath()));
            if(originalLabel == null) {
                originalLabel = '';
            }
            // Append the value with ID.
            //originalValue = originalLabel+(String)originalLead.get('id');
            originalValue = originalLabel;
            // Add the original value and label 
            this.selectionOptions.add(new SelectOption(originalValue, originalLabel));
            for(Lead eachDuplicateLead : duplicateLeads) {
                String duplicateLabel = String.valueOf(eachDuplicateLead.get(field.getFieldPath()));
                Boolean isDisabled = false;
                if(duplicateLabel == null) {
                    duplicateLabel = '';
                    isDisabled = true;
                }  
                String duplicateValue = duplicateLabel;
                //String duplicateValue = duplicateLabel+(String)eachDuplicateLead.get('id');
                mapOfDuplicateValueAndLabels.put(duplicateValue, duplicateLabel);
                // Add the selection option
                this.selectionOptions.add(new SelectOption(duplicateValue, duplicateLabel, isDisabled));

            }
            // This is for a single duplicate, for multiple duplicates, change this line into a loop over the param of list of Leads
            // Business Rules to set the selectedOption. 
            // Rule 1.
            // Set the original value as selected by default.
            this.selectedOption = originalValue;

            // Rule 2. 
            // If there original is blank, set the duplicate as selected by default.
            if(originalLabel == '') {
                // change this to a loop if there are more than one duplicates. 
                for(String eachDuplicateValue : mapOfDuplicateValueAndLabels.keySet()) { 
                    // If the duplicate label is not blank. 
                    if(mapOfDuplicateValueAndLabels.get(eachDuplicateValue) != '') {
                        this.selectedOption = eachDuplicateValue;
                        break;
                    }
                } 
            }
            // Rule 3. ... 4. ... etc. go here.
        } 
    }

    // Helper Methods
    private String makeSelectAllQueryForSObject(String ObjectName) {
        // Initialize setup variables
        // String ObjectName = 'Contact';  // modify as needed
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();

        // Grab the fields from the describe method and append them to the queryString one by one.
        for(String s : objectFields.keySet()) {
           query += ' ' + s + ', ';
        }

        // Some String formatting.
        query = query.trim();

        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }

        // Add related fields or mandatory fields manually to the query. 
        query += ' , Owner.Name, Owner.Email ';

        // Add FROM statement
        query += ' FROM ' + ObjectName;

        // Return the final query
        return query;
    }


     private Lead getLeadById(String Id) {
        String queryString = makeSelectAllQueryForSObject('Lead');
        queryString += ' WHERE Id = \''+ Id +'\'';
        Lead result = null;
        try {
            result = Database.query(queryString);
        }catch (Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }
        return result;
    }

    private List<Lead> getListOfLeadsbyIds(Set<Id> leadIds) {
        List<Lead> returnList = new list<Lead>();
        String queryString = makeSelectAllQueryForSObject('Lead');
        queryString += ' WHERE Id IN :leadIds';
        try {
            returnList = Database.query(queryString);
        }catch (Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }
        return returnList;
    }

    public class FieldsToUpdate {
        String fieldName {get;set;}
        String objectName {get;set;}
        Id recordId {get;set;}
    }

     //Send Lead Notifications.
    public void sendLeadNotification(Lead currentLead, List<Lead> duplicateLeads, MessageType mType) {
        if(mType == MessageType.LeadMerged) {
            sendLeadActionNotification(currentLead, duplicateLeads, 'Leads_Merge_Notification_To_Lead_Owner');
        } else if (mType == MessageType.LeadIgnored) {
            sendLeadActionNotification(currentLead, duplicateLeads, 'Leads_Duplicate_Notification_To_Lead_Owner');
        }
    }



    public void sendLeadActionNotification(Lead currentLead, List<Lead> duplicateLeads, String templateName){
     
        String currentLeadLink = '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentLead.Id + '">' + currentLead.Name + '</a>';
        String leadOwnerName = currentLead.Owner.Name;
        // Get the template.
        EmailTemplate emailTemplate = [SELECT Id, Name, HtmlValue, Subject FROM EmailTemplate Where DeveloperName = :templateName limit 1];
        
        List<Messaging.SingleEmailMessage> listOfMessages = new List<Messaging.SingleEmaiLMessage>();

        for(Lead eachDuplicateLead : duplicateLeads) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String htmlBody = emailTemplate.HtmlValue;
            String duplicateLeadLink = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() +'/' + eachDuplicateLead.Id + '">' + eachDuplicateLead.Name + '</a>';
            
        	htmlBody = htmlBody.replace('{!Lead.Name}', currentLeadLink);
            htmlBody = htmlBody.replace('{!Lead.Owner}', leadOwnerName); 
            htmlBody = htmlBody.replace('{!duplicateLead.Owner.Name}', eachDuplicateLead.Owner.Name);
            htmlBody = htmlBody.replace('{!duplicateLead.Name}', duplicateLeadLink);

            String toEmail = Test.isRunningTest() ? 'gunish.chawla@addisonlee.com': eachDuplicateLead.Owner.Email;
            
            mail.setToAddresses(new String[] {toEmail});
            mail.setReplyTo(currentLead.Owner.Email);
            mail.setSenderDisplayName('AddisonLee Salesforce Team');
            mail.setUseSignature(true);
            mail.setHtmlBody(htmlBody);
            mail.setSubject(emailTemplate.Subject);
            
            if (toEmail != '') {
                listOfMessages.add(mail);
            }
        }

        if(listOfMessages.size() > 0) {
            Messaging.sendEmail(listOfMessages);
        }
    }

    public enum MessageType { LeadMerged, LeadIgnored }
}