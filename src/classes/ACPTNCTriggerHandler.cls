public without sharing class ACPTNCTriggerHandler {
	
	//ACPTNCTTriggerHandlerExtension extensionClass = new ACPTNCTTriggerHandlerExtension();
	//@future(callout=true)
	public void tncCreated(List<AddLee_Terms_Conditions__c> newTNCs){
		// Steps 
		// 1. Loop through all the TNCs
		// 2. Create new Lead Records with T_C_Set__c to true
		// 3. If the TNC was already accepted then set the T_C_Accepted__c = true
		// 4. Call a future method to update all the leads. 

		List<Lead> listOfLeadsToUpdate = new List<Lead>();
		for(AddLee_Terms_Conditions__c newTNC : newTNCs) {
			Lead leadToUpdate = new Lead(Id=newTNC.Internal_Id__c);
			leadToUpdate.T_C_Sent__c = true;
			if(newTNC.Response__c == 'Accepted') {
				leadToUpdate.T_C_Accepted__c = true;
			}
			listOfLeadsToUpdate.add(leadToUpdate);
		}

		List<String> serialisedListOfLeadsToUpdate = new List<String>();
		for(Lead eachLead : listOfLeadsToUpdate) {
			serialisedListOfLeadsToUpdate.add(JSON.serialize(eachLead));
		}

		// A Future method is required while updating a lead because, at this point the lead is still being processed. 
		ACPTNCTriggerHandler.futureLeadUpdate(serialisedListOfLeadsToUpdate);
	}
	
	@future(callout=true)
	public static void futureLeadUpdate(List<String> serialisedLeadsToUpdate) {
		List<Lead> listOfLeadsToUpdate = new List<Lead>();
		for(String eachSearialisedLead: serialisedLeadsToUpdate) {
			listOfLeadsToUpdate.add((Lead)JSON.deserialize(eachSearialisedLead, Lead.class));	
		}
		update listOfLeadsToUpdate;	
	
	}


	//@future(callout=true)
	public void tncUpdated( Map<Id, AddLee_Terms_Conditions__c> oldTNCMap, Map<Id, AddLee_Terms_Conditions__c> newTNCMap ){
		set<Id> leadIDsToUpdate = new set<Id>(); 
		for(AddLee_Terms_Conditions__c thisTNC : newTNCMap.values()){
			if( thisTNC.Response__c == 'Accepted' && oldTNCMap.get(thisTNC.id).Response__c != 'Accepted' ){
				thisTNC.External_Id__c = '';
				// Collect the IDs of all the leads which need to be updated to set the flag. 
				leadIDsToUpdate.add(thisTNC.Internal_Id__c);
			}
		}

		// Update the corresponding leads. This is required to fire the triggers attached to the lead, so that some leads can be automatically converted. 
		list<Lead> listOfLeadsToUpdate = new List<Lead>();
		for(Id eachLeadId : leadIDsToUpdate) {
			Lead leadToUpdate = new Lead(Id=eachLeadId);
			leadToUpdate.T_C_Accepted__c = true;
			listOfLeadsToUpdate.add(leadToUpdate);
		}
		
		update listOfLeadsToUpdate;
	}
	
	//public static void sendTermsAndConditionsEmail(AddLee_Terms_Conditions__c newTNC, Lead newLead){
 	//  String toEmail = Test.isRunningTest() ? 'benson.daniel@addisonlee.com': newLead.Email;
 	//  String ccEmail = Test.isRunningTest() ? 'benson.daniel@addisonlee.com': newLead.Owner.Email;
 	//  EmailTemplate et = [SELECT Id, Name, HtmlValue FROM EmailTemplate Where Name = 'Leads: Terms and Conditions' limit 1];
 	//  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	//	String[] toAddresses = new String[] {toEmail}; 
	//	String[] ccAddresses = new String[] {ccEmail};
	//	String htmlBody = et.HtmlValue;
	//	htmlBody = htmlBody.replace('{!AddLee_Terms_Conditions__c.External_Id__c}', newTNC.External_Id__c);
	//	htmlBody = htmlBody.replace('{!Lead.Name}', newLead.Name);
	//	mail.setToAddresses(toAddresses);
	//	mail.setCcAddresses(ccAddresses);
	//	mail.setReplyTo(newLead.Owner.Email);
	//	mail.setSenderDisplayName('AddisonLee Team');
	//	mail.setBccSender(true);
	//	mail.setUseSignature(true);
	//	mail.setHtmlBody(htmlBody);
		
	//	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 	//  }
	
}