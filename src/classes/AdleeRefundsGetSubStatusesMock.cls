@isTest
global class AdleeRefundsGetSubStatusesMock implements WebserviceMock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.subStatusResponse respElement =  new Addlee_refunds.subStatusResponse();
       respElement.subStatuses = new List<Addlee_refunds.subStatus>();
               Addlee_refunds.subStatus substatus = new Addlee_refunds.subStatus();
               substatus.code = '43543';
               substatus.id = 54654;
               substatus.name = 'test';
         respElement.subStatuses.add(substatus);      
         
       response.put('response_x', respElement); 
   }
    
}