public without sharing class AddLee_BankCapture_Controller {

    public Lead leadRecord { get; set; }

    public Boolean isApexTestRunning { get; set; }

    ApexPages.StandardController controller;

    Account_Creation_Settings__c accountCreationSettings { get; set; }

    public AddLee_BankCapture_Controller(ApexPages.StandardController controller) {
        leadRecord = (Lead)controller.getRecord();
        leadRecord = [SELECT Id, FirstName, LastName, Street, Country, PostalCode, State, City, Bank_Sort_Code__c, Bank_Account_Name__c, Bank_Account_Number__c, Bank__c, Bank_Date_of_DD__c, Bank_Details_Captured__c FROM LEAD WHERE ID =: leadRecord.Id ];
        leadRecord.Bank_Account_Number__c = '';
        leadRecord.Bank_Sort_Code__c = '';
        this.controller = controller;
        accountCreationSettings = Account_Creation_Settings__c.getInstance('credentials');
    }

    public PageReference validate(){
        Addlee_PostcodeAnywhereBankValidity.WebServiceSoap request = new Addlee_PostcodeAnywhereBankValidity.WebServiceSoap();
        Addlee_PostcodeAnywhereBankValidity.ArrayOfResults results = new Addlee_PostcodeAnywhereBankValidity.ArrayOfResults();
        if(isApexTestRunning != null && isApexTestRunning){
            Addlee_PostcodeAnywhereBankValidity.Results result = new Addlee_PostcodeAnywhereBankValidity.Results();
            result.IsCorrect = true;
            result.ContactAddressLine1 = 'TestaddressLine1';
            result.ContactPostcode = 'testpostcode';
            result.ContactPostTown = 'testPostTown';
            result.Bank = 'TestBank';
            result.IsDirectDebitCapable = true;
            results.Results = new List<Addlee_PostcodeAnywhereBankValidity.Results>();
            results.results.add(result);
        }else{
            results = request.validate(accountCreationSettings.pcaKey__c,leadRecord.Bank_Account_Number__c, leadRecord.Bank_Sort_Code__c);  
        }
        //System.DEBUG(leadRecord.Bank_Account_Number__c);
        //System.DEBUG(leadRecord.Bank_Sort_Code__c);

        if(results.results[0].IsCorrect && results.results[0].IsDirectDebitCapable){
            leadRecord.Bank__c = results.results[0].Bank;
            leadRecord.Bank_Date_of_DD__c = System.today();
            leadRecord.Bank_Address__c = results.results[0].ContactAddressLine1 + '\n' + results.results[0].ContactPostcode + '\n' + results.results[0].ContactPostTown;
            leadRecord.Name_of_Approver__c = leadRecord.FirstName + ' ' + leadRecord.LastName;
            leadRecord.Bank_Details_Captured__c = true;
            leadRecord.TECH_Bank_Encryption_Set__c = false;
            update leadRecord;
            return new PageReference('/apex/CreditCheckPass?id=' + leadRecord.Id);
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,results.results[0].StatusInformation);
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        //System.debug(results);
        //System.debug(leadRecord);
        
        //return null;
    }
}