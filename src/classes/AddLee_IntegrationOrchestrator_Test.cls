/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddLee_IntegrationOrchestrator_Test {
	
	@isTest
    static void AddLee_IntegrationOrchestrator_isAbleToCreateAccountInShamrock() {
    	//AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort();
    	//String sessionId = respElement.Login(AddLee_IntegrationUtility.getUsername(),AddLee_IntegrationUtility.getPassword());
    	//AddLee_SoapIntegration_Test_v1.customer createdCustomer = new AddLee_SoapIntegration_Test_v1.customer('test');
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Test.startTest();
	    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
	    	AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
	    	AddLee_SoapIntegration_Test_v2_Fix.customerResponse customerResponse = respElement.CreateCustomer('test', null, null);
	    	System.assertEquals('Test Create Response',customerResponse.description);
	    Test.stopTest();
        // TO DO: implement unit test
    }
    
    @isTest
    static void AddLee_IntegrationOrchestrator_isAbleToCreateAccountInShamrockBadResponseSeesionExpire() {
    	//AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort();
    	//String sessionId = respElement.Login(AddLee_IntegrationUtility.getUsername(),AddLee_IntegrationUtility.getPassword());
    	//AddLee_SoapIntegration_Test_v1.customer createdCustomer = new AddLee_SoapIntegration_Test_v1.customer('test');
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Test.startTest();
	    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
	    	AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
	    	AddLee_SoapIntegration_Test_v2_Fix.customerResponse customerResponse = respElement.CreateCustomer('testresponse', null, null);
	    	System.assertEquals('Session expired',customerResponse.description);
	    Test.stopTest();
        // TO DO: implement unit test
    }
    
    @isTest
    static void AddLee_IntegrationOrchestrator_isAbleToAmendAccountInShamrock() {
    	//AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort();
    	//String sessionId = respElement.Login(AddLee_IntegrationUtility.getUsername(),AddLee_IntegrationUtility.getPassword());
    	//AddLee_SoapIntegration_Test_v1.customer createdCustomer = new AddLee_SoapIntegration_Test_v1.customer('test');
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Test.startTest();
	    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
	    	AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
	    	AddLee_SoapIntegration_Test_v2_Fix.customerResponse customerResponse = respElement.AmendCustomer('test', null, null);
	    	System.assertEquals('Test Amend Response',customerResponse.description);
	    Test.stopTest();
        // TO DO: implement unit test
    }
    
    @isTest
    static void AddLee_IntegrationOrchestrator_isAbleToLoginInShamrock() {
    	//AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort();
    	//String sessionId = respElement.Login(AddLee_IntegrationUtility.getUsername(),AddLee_IntegrationUtility.getPassword());
    	//AddLee_SoapIntegration_Test_v1.customer createdCustomer = new AddLee_SoapIntegration_Test_v1.customer('test');
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Test.startTest();
	    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
	    	AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
	    	String customerResponse = respElement.Login('test', null);
	    	System.assertEquals('Test Login Response',customerResponse);
	    Test.stopTest();
        // TO DO: implement unit test
    }
    
    @isTest
    static void AddLee_IntegrationOrchestrator_isAbleToLogoutOfShamrock() {
    	//AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort();
    	//String sessionId = respElement.Login(AddLee_IntegrationUtility.getUsername(),AddLee_IntegrationUtility.getPassword());
    	//AddLee_SoapIntegration_Test_v1.customer createdCustomer = new AddLee_SoapIntegration_Test_v1.customer('test');
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Test.startTest();
	    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
	    	AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort respElement = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
	    	respElement.Logout();
	    Test.stopTest();
        // TO DO: implement unit test
    }
    
    @isTest
    static void AddLee_IntegrationOrchestrator_isAbleToHandleInsert(){
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Log__c testLog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
    	
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
    		insert testLog;
    	Test.stopTest();
    	
    	Integer actualValue = [SELECT count() FROM Log__c WHERE id =:testLog.Id];
    	System.assertEquals(1,actualValue);
    }
    
    @isTest
    static void AddLee_IntegrationOrchestrator_isAbleToHandleUpdate(){
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Log__c testLog = AddLee_Trigger_Test_Utils.createLogs(1,'Update')[0];
    	insert testLog;
    	testLog.Status__c = 'Waiting';
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
    		update testLog;
    	Test.stopTest();
    	
    	Integer actualValue = [SELECT count() FROM Log__c WHERE id =:testLog.Id];
    	System.assertEquals(1,actualValue);
    }
}