@isTest
private class AddLee_PaymentCapture_Controller_Test {

    static testMethod void updateCardDetailsTest() {
    	
		AddLee_Trigger_Test_Utils.insertCustomSettings();
    	insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id from Lead limit 1];
    	Test.setCurrentPage(new PageReference('/apex/PaymentCapture?id='+testLead.Id+'&type=card'));
    	
        AddLee_PaymentCapture_Controller theController = new AddLee_PaymentCapture_Controller();
        String personal = theController.getIsPersonalLead();
    	AddLee_PaymentCapture_Controller.retreiveLead(testLead.Id);
    	Lead thisLeadDetail = theController.getLeadDetail();
    	String THISmONTH = theController.getThisMonth();
    	Date cardExpiry = AddLee_PaymentCapture_Controller.getCardExpirayDate('2015-06');
    	String jsonInit = theController.getInitVariables();
    	
        Map<String, String> cardDetails = new Map<String, String>();
        cardDetails.put('recordId',testLead.id);
        cardDetails.put('crdHldrName','Test Card Holder Name');
        cardDetails.put('crdBillAddr','Test Card Address');
        cardDetails.put('crdNumber','4000 1234 1234 1234');
        cardDetails.put('crdExpDate','2015-06');
        AddLee_PaymentCapture_Controller.isApexTestRunning = true;
        Map<Object, Object> response = AddLee_PaymentCapture_Controller.updateCardDetails(cardDetails);
        system.assertEquals(String.valueOf(response.get('sfSuccess')),'true');
        
    }
    
    static testMethod void updateBankDetailsTest() {
    	
		AddLee_Trigger_Test_Utils.insertCustomSettings();
    	insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id from Lead limit 1];
    	Test.setCurrentPage(new PageReference('/apex/PaymentCapture?id='+testLead.Id+'&type=bank'));
        AddLee_PaymentCapture_Controller theController = new AddLee_PaymentCapture_Controller();
        Map<String, String> cardDetails = new Map<String, String>();
        cardDetails.put('recordId',testLead.id);
        cardDetails.put('bankAccName','Test Account Name');
        cardDetails.put('bankSrtCode','000099');
        cardDetails.put('bankAccNumber','40001234');

        AddLee_PaymentCapture_Controller.isApexTestRunning = true;
        Map<Object, Object> response = AddLee_PaymentCapture_Controller.updateBankDetails(cardDetails);
        system.assertEquals(String.valueOf(response.get('sfSuccess')),'true');
        
    }
    
}