global without sharing class AddLee_AccountConversion_Pulse implements Schedulable {
    Integer intervalMinutes;
    public AddLee_AccountConversion_Pulse(Integer intervalMinutes) {
        this.intervalMinutes = intervalMinutes;
    }
    public void execute(SchedulableContext sc) {
        
        // Converted this Batch Code to a Trigger Based Workflow. 
        
        //// Re-schedule ourself to run again in "intervalMinutes" time
        //DateTime now  = DateTime.now();
        //DateTime nextRunTime = now.addMinutes(intervalMinutes);
        //String cronString = '' + nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + 
        //    nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + 
        //    nextRunTime.month() + ' ? ' + nextRunTime.year(); 
        //System.schedule(AddLee_AccountConversion_Pulse.class.getName() + '-' + now.format(), cronString, new AddLee_AccountConversion_Pulse(intervalMinutes));
        //// Abort the current job
        //Id jobId = sc.getTriggerId();
        //System.abortJob(jobId);     
        //// Launch a batch job or call a future method to do the actual work
        //Map<Id, Lead> readyToConvertLeadMap = new Map<Id, Lead>([SELECT Id, createdById, isConverted, Convert_To_Account__c, RAG_Status__c, Bank_Details_Captured__c 
        //										FROM Lead 
        //										WHERE Convert_To_Account__c = true
        //										And isConverted = false]);
        									
        //AddLee_Lead_Conversion.convertLeadonSchedule( readyToConvertLeadMap.keySet() );
        			
    }
}