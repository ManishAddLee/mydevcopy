//Contact opt out handler 

public  class ContactOptout {
	List<Account> acc = new List<Account> ();
	//list<ID> contactids = new list<ID>();
	 set<ID> contactids = new set<ID>();
     set<ID> emailids = new set<ID>();
     set<ID> smaids = new set<ID>();
    List<String> accconids =new List<String>();
    list<Contact> emailcon = new list<Contact>();
    list<Contact> smscon = new list<Contact>();
    List<Contact> finalcon = new list<Contact>();
    Map<Id, Contact> mapContact = new Map<ID, Contact>();
   	public void optoutHandler(list<Contact> con){
		
        for ( Contact c : con){
            if(c.AccountID != null){
            String str = String.valueOf(c.AccountID);
            
            String stracc = str.substring(0,str.length()-3);
            
            accconids.add(stracc);
            mapContact.put(c.AccountID,c);
            
             
        }
         
        acc =[select id ,Name,OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c,
              Opt_Out_Of_Email_Marketing__c,
              Opt_Out_Of_SMS_Marketing__c
              from Account where Id IN :accconids ];
        
        system.debug('acc+++'+acc);
        for(Account optacc : acc){
            system.debug('optacc.OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c'+optacc.OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c);
             system.debug('optacc.Opt_Out_Of_Email_Marketing__c'+optacc.Opt_Out_Of_Email_Marketing__c);
              system.debug('optacc.Opt_Out_Of_SMS_Marketing__c'+optacc.Opt_Out_Of_SMS_Marketing__c);
              system.debug('optacc.id' + optacc.id);
              system.debug('mapContact' + mapContact.keySet());
            
        if (mapContact.containsKey(optacc.Id) && optacc.OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c != false ){
        	system.debug('eneter into all loop');
            contactids.add(optacc.id);
            if(contactids.size()> 0){
            	for(Contact conupdate : con){
            		if(contactids.contains(conupdate.AccountID)){
            			conupdate.HasOptedOutOfEmail = true;
                        conupdate.Optout_All__c = true;
                        conupdate.SMS_Opt_Out__c = true;
                        
            		}
            	}
            }
            
           
        }
        else if(mapContact.containsKey(optacc.Id) && optacc.Opt_Out_Of_Email_Marketing__c != false){
            emailids.add(optacc.id);
            if (emailids.size()>0) {
            	for(Contact emailcon : con){
            		if(emailids.contains(emailcon.AccountID)){
            			emailcon.HasOptedOutOfEmail                     = true;
               			emailcon.Add_Lib_Lifestyle_Content_Email__c     = false;
               			emailcon.News_Letters_And_Updates_Email__c      = false; 
               			emailcon.Offers_Discounts_Competitions_Email__c = false;
               			emailcon.Surveys_And_Research_Email__c          = false;
            			
            		}
            	}
            }
            
        }
        else if(mapContact.containsKey(optacc.Id) && optacc.Opt_Out_Of_SMS_Marketing__c == true){
            smaids.add(optacc.id);
            if(smaids.size() >0){
            	for(Contact smscon : con ){
            		if(smaids.contains(smscon.AccountID)){
            			 smscon.SMS_Opt_Out__c                         = true;
              			 smscon.Add_Lib_Lifestyle_Content_SMS__c       = false;
               			 smscon.News_Letters_And_Updates_SMS__c        = false; 
               			 smscon.Offers_Discounts_Competitions_SMS__c   = false;
               			 smscon.Surveys_And_Research_SMS__c            = false;
            			
            		}
            	}
            }
        }
    }
    
        
     }
    
     	
    
    }
   
	}