Public Class LeadCompetitor{
Public JNC_CompetitorLead__c JNC{get;set;}
Public String LeadId;
Public LeadCompetitor(){
LeadId=ApexPages.currentPage().getParameters().get('id');
JNC = [Select Id,Competitor__c,Supplier_or_Competitor__c,Lead__c from JNC_CompetitorLead__c limit 1];
JNC.Competitor__c = null;
JNC.Supplier_or_Competitor__c = '';
JNC.Lead__c = LeadId;
}
Public Void SaveRecord(){
JNC_CompetitorLead__c jncCOMP = new JNC_CompetitorLead__c();
JncCOMP.Competitor__c = JNC.Competitor__c;
JncCOMP.Supplier_or_Competitor__c = JNC.Supplier_or_Competitor__c;
JncCOMP.Lead__c = JNC.Lead__c;
Insert JncCOMP;
}
}