public class CreditCheckPassExtension {

    public Lead lead { get; set; }
    
    public String creditCheckSuccesful { get; set; }
    
    public String creditCheckUnSuccesful { get; set; }
    
    public CreditCheckPassExtension(ApexPages.StandardController controller) {
        lead = (Lead)controller.getRecord();
        creditCheckSuccesful = '* Credit check successful <br />' ;
        creditCheckSuccesful += '* Customer has been emailed terms & conditions/payment terms for acceptance <br />';
        creditCheckSuccesful += '* Account will open automatically once customer accepts terms & conditions/payment terms <br />';
        creditCheckUnSuccesful = '* Credit check unsuccessful. Referred for a manual review <br />';
        creditCheckUnSuccesful += '* Customer has been emailed terms & conditions/payment terms for acceptance <br />';
        creditCheckUnSuccesful += '* Account will open automatically once credit check has been approved and customer accepts terms & conditions/payment terms <br />';
    }
    
    public Pagereference redirectToLead(){
        return new Pagereference('/'+ lead.Id);
    }

}