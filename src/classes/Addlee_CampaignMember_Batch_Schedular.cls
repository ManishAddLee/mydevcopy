global class Addlee_CampaignMember_Batch_Schedular implements Schedulable {
	global void execute(SchedulableContext sc) {
		Addlee_CampaignMember_Account_Batch b = new Addlee_CampaignMember_Account_Batch();
		database.executebatch(b);
	}
}