// Schedule class for A ccountservice type 

global class Servicetype_Schedular implements Schedulable {
	global void execute (SchedulableContext sc) {
		CalculateServiceTypeOnAccount csa = new CalculateServiceTypeOnAccount();
	 	csa.Query = 'select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where  Reason_for_opening_account__c = \'Business\' and Consumer_Account_Number__c != \'1\' and Date_of_Last_Booking__c != null and Total_Amount_of_Bookings__c <= 49000' ;
	 	Database.executeBatch(csa,20);
		
	}
	

}