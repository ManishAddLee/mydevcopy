public class parseEmailContent {
    public static final string DRIVER_RATING = 'Driver Rating:';
    public static final string OVERALL_RATING = 'Overall Rating:';
    public static final string DRIVER_COMMENTS = 'Comments:';
    public static final string JOB = 'job';
    // &#9733; is html equilant to black star ★
    Public static final string RATING_CHAR = '&#9733;';
    Public static final Integer MAX_AVG_RATING = 3;
    Public static final string MAILTO = '<a href="mailto:';
    Public static final string PHONE = 'Phone:';
    
    public static response parseEmail(Case cse, EmailMessage message){
        List<String> keyWords = new List<String>{'lost','lost property','incident','accident','racist','abuse','homophobic','rape'};
        string driverRatingStr;
        string overAllRatingStr;
        response res = new response();
        if(message.htmlBody.containsIgnoreCase(DRIVER_RATING)) driverRatingStr = message.htmlBody.substring(message.htmlBody.indexOf(DRIVER_RATING), message.htmlBody.indexOf(OVERALL_RATING));
        if(message.htmlBody.containsIgnoreCase(OVERALL_RATING))overAllRatingStr = message.htmlBody.substring(message.htmlBody.indexOf(OVERALL_RATING), message.htmlBody.indexOf(DRIVER_COMMENTS));
        if(message.Subject.containsIgnoreCase(JOB)) {
            List<String> ls = message.Subject.splitByCharacterTypeCamelCase();
            if(!ls.isEmpty()){
                for(String s : ls){
                    if(s.isNumeric()) res.jobNumber = s;
                }
            }
        }
        res.driverRating = driverRatingStr.countMatches(RATING_CHAR);
        res.overallRating = overAllRatingStr.countMatches(RATING_CHAR);  
        res.externalId = getExternalId(); 
        res.avgRating = ( Double.valueOf(res.driverRating) + Double.valueOf(res.overallRating) )/2; 
        
        // Extracting comments
        String commStr = message.htmlBody.substring(message.htmlBody.IndexOf(DRIVER_COMMENTS) + DRIVER_COMMENTS.length(), message.htmlBody.length());
        List<String> strings = commStr.split('\n');
        /*if(strings[0].containsOnly('</font></div>') || strings[0].containsOnly('</span></div></div></div>') || strings[0].containsOnly('</span></p>') || strings[0].contains('</span>') || strings[0].contains('</strong> <br/>')){
            res.sendCommentEmail = true;
        }else{
            res.sendCommentEmail = false;
        } */
        System.debug('MTDebug strings : ' + strings);
        System.debug('MTDebug strings[1] : ' + strings[1]);
        if(strings[1].containsOnly('                     <br/>')){
            res.sendCommentEmail = true;
        }else{
            res.sendCommentEmail = false;
        }
        
        // Extracting customer emial
        String mailToStr = message.htmlBody.substring(message.htmlBody.IndexOf(MAILTO), message.htmlBody.indexOf(PHONE));
        System.debug('MTDebug mailToStr : ' + mailToStr);
        List<String> mailToStrs = mailToStr.split('\n');
        for(string s : mailToStrs){
            if(s.containsIgnoreCase(MAILTO)){
                res.custEmail = s.substring(s.IndexOf(MAILTO) + MAILTO.length(),s.IndexOf('\">'));
            }
        }
        System.debug('MTDebug res.custEmail : ' + res.custEmail);
        return res;
    }
    
    private static String getExternalId(){
    	Integer len = 14;
	    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
	    String randStr = '';
	    while (randStr.length() < len) {
	       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
	       randStr += chars.substring(idx, idx+1);
	    }
		return randStr; 
    }
    
    public class response{
        public Integer driverRating;
        public Integer overallRating;
        public Double avgRating;
        public Boolean sendCommentEmail;
        public String externalId;
        public String jobNumber;
        public String custEmail;
    }
}