@isTEst
global class AddleeRefundsGetGradesMock implements WebServicemock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.gradeResponse respElement =  new Addlee_refunds.gradeResponse();
         respElement.grades = new List<Addlee_refunds.grade>();    
         Addlee_refunds.grade testGrade = new Addlee_refunds.grade();
         testGrade.id = 5675;
         testGrade.name = 'grade';
         respElement.grades.add(testGrade);     
       response.put('response_x', respElement); 
   }
}