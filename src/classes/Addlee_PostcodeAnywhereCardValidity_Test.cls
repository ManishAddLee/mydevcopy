@isTest
private class Addlee_PostcodeAnywhereCardValidity_Test {

    static testMethod void AddLee_PostcodeAnywhereCardValidity_validateTestBankAccount() {
        Addlee_PostcodeAnywhereCardValidity.WebServiceSoap request = new Addlee_PostcodeAnywhereCardValidity.WebServiceSoap();
    
      	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());  
      	Addlee_PostcodeAnywhereCardValidity.ArrayOfResults results = request.validate('test','testcardnumber');  
      	System.assertEquals(results.Results[0].IsValid, true);
    }
}