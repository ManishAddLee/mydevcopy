@isTest

public class TestDataFactory {

    public static Account createTestAccount(String accName, String recordTypeName){
    	Account testAccount = new Account();
    	testAccount.Name = accName;
    	testAccount.RecordTypeId = getRecordTypeId( recordTypeName );
    	testAccount.Industry = 'Professional, Scientific and Technical';
    	testAccount.Industry_Sub_Sector__c = 'Management Consulting';
    	testAccount.ShippingStreet = '7 Test Street, Test City TS7 7ST';
    	return testAccount;
    }
    
    public static AddLee_Terms_Conditions__c createTnC(Lead testLead){
    	AddLee_Terms_Conditions__c testTnC = new AddLee_Terms_Conditions__c();
    	testTnC.Internal_Id__c = testLead.Id;
    	testTnC.External_Id__c = '8yMsYrqp501qqe';
    	return testTnC;
    }
       
    public static Lead createTestLead(String leadName){
    	Lead testLead = new Lead(LastName = leadName, Email = 'benson.daniel@addisonlee.com', Company = 'Test Company', PostalCode = 'SE1 0HS', Payment_Type__c = 'Direct Debit');
    	return testLead;
    }
    
    private static Id getRecordTypeId(String recordTypeName){
    	Id recordTypeId;
    	try{
	    	recordTypeId = [Select Id, Name
    							from RecordType 
    							Where Name =:recordTypeName  and SObjectType = 'Account'
    							limit 1].Id;
    	} catch (Exception e){
    		system.debug(e);
    		return recordTypeId;
    	}
    	return recordTypeId;
    }
    
}