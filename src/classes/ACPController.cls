public with sharing class ACPController {
    
    @RemoteAction
    public static List<Map<Object, Object>> searchPersonAccountsAndLeads(Map<String,String> inputString){
        List<Map<Object, Object>> dto = new List<Map<Object, Object>>();
        String searchTerm = inputString.get('term');
        searchTerm = '%' + searchTerm + '%';
        system.debug('The search term is '+searchTerm);
        String currentUserProfile = [Select id, Name from Profile Where Id =:UserInfo.getProfileId() limit 1].Name;
        system.debug('Your profile name is '+currentUserProfile);
        set<String> salesLedgerSet = new set<String>();
        if(!currentUserProfile.startsWith('WestOne') && (currentUserProfile != 'System Administrator') ){//Any other than Westone
            salesLedgerSet.add('WestOne Sales Ledger');
        } else if(currentUserProfile == 'System Administrator'){
            //Do nothing
            system.debug('You are a system admin');
        } else {
            salesLedgerSet = getAccountSalesLedgerSet();
            salesLedgerSet.remove('WestOne Sales Ledger');
        }
        system.debug('Salesledgers not in '+salesLedgerSet);

        List<Account> accountSearchResults = [SELECT Id, Name, FirstName, LastName, PersonEmail, Birthdate__pc, RecordTypeId,
                                                    ShippingStreet,Account_Manager__c,Owner.Name, LastModifiedDate, 
                                                    Account_Status__c 
                                                FROM Account 
                                                WHERE (FirstName Like :searchTerm 
                                                OR LastName Like :searchTerm ) 
                                                AND isPersonAccount = true 
                                                And SHM_salesLedger_name__c Not IN :salesLedgerSet limit 10];
        List<Lead> leadSearchResults = [SELECT Id, Name, FirstName, LastName, phone,Email,Birthdate__c,PostalCode, State,
                                                Salutation,D_B_Recommendation__c,D_B_Credit_Limit__c,Street,
                                                Days_since_last_Credit_Check__c, Last_Credit_Checked_Date__c, 
                                                Owner.Name, LastModifiedDate, CompanyDunsNumber
                                            FROM Lead 
                                            WHERE (isConverted = false AND Company = Null) 
                                            AND (Not Status Like 'Closed%')
                                            AND (FirstName Like :searchTerm OR LastName Like :searchTerm) limit 10];
        Map<Object, Object> projects;
        for(Lead thisLead : leadSearchResults){
            projects = new Map<Object, Object>();
            projects.put('id',thisLead.id);
            projects.put('value',thisLead.Name);
            projects.put('label',thisLead.Name);
            projects.put('ownrName',thisLead.Owner.Name);
            projects.put('email',thisLead.Email);
            projects.put('salutation',thisLead.Salutation);
            projects.put('firstName',thisLead.FirstName);
            projects.put('lastName',thisLead.LastName);
            projects.put('birthDate',String.valueOf(thisLead.Birthdate__c));
            projects.put('desc',thisLead.Street);
            projects.put('postCode',thisLead.PostalCode);
            projects.put('town',thisLead.State);
            projects.put('duns',thisLead.CompanyDunsNumber);
            projects.put('daysSinceCC',thisLead.Days_since_last_Credit_Check__c);
            projects.put('type','lead');
            dto.add(projects);
        }
        for(Account thisAccount : accountSearchResults){
            projects = new Map<Object, Object>();
            projects.put('id',thisAccount.id);
            projects.put('recordTypeId',thisAccount.RecordTypeId);
            projects.put('value',thisAccount.Name);
            projects.put('label',thisAccount.Name);
            projects.put('mgrName',thisAccount.Account_Manager__c);
            projects.put('ownrName',thisAccount.Owner.Name);
            projects.put('desc',thisAccount.ShippingStreet);
            projects.put('type','account');
            dto.add(projects);
        }
        return dto;
    }
    
    @RemoteAction
    public static List<Map<Object, Object>> searchAccountsAndLeads(Map<String,String> inputString){
        List<Map<Object, Object>> dto = new List<Map<Object, Object>>();
        String searchTerm = inputString.get('term');
        searchTerm = '%' + searchTerm + '%';
        system.debug('The search term is '+searchTerm);
        String currentUserProfile = [Select id, Name from Profile Where Id =:UserInfo.getProfileId() limit 1].Name;
        system.debug('Your profile name is '+currentUserProfile);
        set<String> salesLedgerSet = new set<String>();
        if(!currentUserProfile.startsWith('WestOne') && (currentUserProfile != 'System Administrator') ){//Any other than Westone
            salesLedgerSet.add('WestOne Sales Ledger');
        } else if(currentUserProfile == 'System Administrator'){
            //Do nothing
            system.debug('You are a system admin');
        } else {
            salesLedgerSet = getAccountSalesLedgerSet();
            salesLedgerSet.remove('WestOne Sales Ledger');
        }
        system.debug('Salesledgers not in '+salesLedgerSet);
        List<Account> accountSearchResults = [Select id,Name,RecordTypeId,ShippingStreet,Account_Manager__c,Owner.Name 
                                                from Account
                                                Where Name Like :searchTerm 
                                                And SHM_salesLedger_name__c Not IN :salesLedgerSet
                                                limit 10];
        List<Lead> leadSearchResults = [Select Id, Name, Street, Owner.Name, CompanyDunsNumber, PostalCode, State,
                                               Company, Last_Credit_Checked_Date__c, Days_since_last_Credit_Check__c
                                        From Lead
                                        Where Company Like :searchTerm 
                                        And (Not Status Like 'Closed%') limit 10];
        Map<Object, Object> projects;
        for(Lead thisLead : leadSearchResults){
            projects = new Map<Object, Object>();
            projects.put('id',thisLead.id);
            projects.put('value',thisLead.Name);
            projects.put('label',thisLead.Name);
            projects.put('ownrName',thisLead.Owner.Name);
            projects.put('company',thisLead.Company);
            projects.put('desc',thisLead.Street);
            projects.put('postCode',thisLead.PostalCode);
            projects.put('town',thisLead.State);
            projects.put('duns',thisLead.CompanyDunsNumber);
            projects.put('daysSinceCC',thisLead.Days_since_last_Credit_Check__c);
            projects.put('type','lead');
            dto.add(projects);
        }
        for(Account thisAccount : accountSearchResults){
            projects = new Map<Object, Object>();
            projects.put('id',thisAccount.id);
            projects.put('recordTypeId',thisAccount.RecordTypeId);
            projects.put('value',thisAccount.Name);
            projects.put('label',thisAccount.Name);
            projects.put('mgrName',thisAccount.Account_Manager__c);
            projects.put('ownrName',thisAccount.Owner.Name);
            projects.put('desc',thisAccount.ShippingStreet);
            projects.put('type','account');
            dto.add(projects);
        }
        return dto;
    }
    @TestVisible  
    private static set<String> getAccountSalesLedgerSet(){
        set<String> salesLedgerSet = new set<String>();
        
        Schema.DescribeFieldResult fieldResult = Account.SHM_salesLedger_name__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
            salesLedgerSet.add(f.getValue());
        }
        
        return salesLedgerSet;
    }
    
}