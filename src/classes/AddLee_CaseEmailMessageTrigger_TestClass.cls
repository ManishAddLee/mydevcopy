@isTest
private class AddLee_CaseEmailMessageTrigger_TestClass {
    /**
    * @Description: Test class to test AddLee_Case(before insert and before update) and AddLee_EmailMessage(after insert) triggers, specifically AddLee_CaseHelper
    *(associateCaseAccountEntitlements,processCases,GetRecordTypeId), AddLee_EmailMessageHelper(AssignAccount)
    */
    
    Static testMethod void validateTriggers() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
               
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();


        List<EmailAddressAccountNumberMappings__c> emailAddressAccountMappings = new List<EmailAddressAccountNumberMappings__c>();
        emailAddressAccountMappings.add(new EmailAddressAccountNumberMappings__c(Account_Number__c='1' , To_Email__c= 'example0@testclass.com', Name='Setting 1'));
        emailAddressAccountMappings.add(new EmailAddressAccountNumberMappings__c(Account_Number__c='2' , To_Email__c= 'example1@testclass.com', Name='Setting 2'));
        emailAddressAccountMappings.add(new EmailAddressAccountNumberMappings__c(Account_Number__c='3' , To_Email__c= 'example2@testclass.com', Name='Setting 3'));
        emailAddressAccountMappings.add(new EmailAddressAccountNumberMappings__c(Account_Number__c='4' , To_Email__c= 'example3@testclass.com', Name='Setting 4'));
        insert emailAddressAccountMappings;

        List<Account> accountsList = new List<Account>();
        accountsList.add(new Account(Name = 'Entitlement Account 24' , Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        accountsList.add(new Account(Name = 'Entitlement Account 48' , Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        accountsList.add(new Account(Name = 'Entitlement Account 72' , Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        accountsList.add(new Account(Name = 'example0@testclass.com Account',exsistInShamrock__c=true,Account_Number__c='1', Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        accountsList.add(new Account(Name = 'example1@testclass.com Account',Account_Managed__c= true ,exsistInShamrock__c=true, Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting')); // no entitlement account but managed
        accountsList.add(new Account(Name = 'Test Account' , Industry = 'Professional, Scientific and Technical',exsistInShamrock__c=true, Industry_Sub_Sector__c = 'Management Consulting'));
        accountsList.add(new Account(Name = 'Test Account1' , Industry = 'Professional, Scientific and Technical',exsistInShamrock__c=true, Industry_Sub_Sector__c = 'Management Consulting'));

        insert accountsList;

        List<Account> childAccountsList = new List<Account>();
        childAccountsList.add(new Account(Name = 'example0@testclass.com Accounts child', ParentId=accountsList[3].Id,Account_Number__c='5', Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        childAccountsList.add(new Account(Name = 'example1@testclass.com Accounts child', ParentId=accountsList[4].Id,Account_Number__c='2', Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        childAccountsList.add(new Account(Name = 'example2@testclass.com Accounts child', ParentId=accountsList[4].Id,Account_Number__c='3', Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        childAccountsList.add(new Account(Name = 'Test Account1 child', ParentId=accountsList[5].Id,Account_Managed__c= true, Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        childAccountsList.add(new Account(Name = 'Test Account1 child', ParentId=accountsList[5].Id, Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));

        Insert childAccountsList;

        List<Entitlement> entitlementList= new List<Entitlement>();
        entitlementList.add(new Entitlement(Name='Account 24',AccountId=accountsList[0].Id,StartDate=system.Today().addDays(-1)));
        entitlementList.add(new Entitlement(Name='Account  48',AccountId=accountsList[1].Id,StartDate=system.Today().addDays(-1)));
        entitlementList.add(new Entitlement(Name='Account 72',AccountId=accountsList[2].Id,StartDate=system.Today().addDays(-1)));
        entitlementList.add(new Entitlement(Name='Test Account',AccountId=childAccountsList[0].Id,StartDate=system.Today().addDays(-1))); // Managed account case
        entitlementList.add(new Entitlement(Name='Test Account',AccountId=childAccountsList[2].Id,StartDate=system.Today().addDays(-1))); // Managed account case
        entitlementList.add(new Entitlement(Name='Account Entitlement1',AccountId=accountsList[5].Id,StartDate=system.Today().addDays(-1)));
        entitlementList.add(new Entitlement(Name='Account Entitlement2',AccountId=childAccountsList[4].Id,StartDate=system.Today().addDays(-1)));
        insert entitlementList;
        

        List<Case> casesToInsert = new List<Case>();

        casesToInsert.add(new Case(Status='New',Origin='WestOne',RecordTypeId=caseRecordTypeId ));//0 72 hr entitlement from generic account
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//1 for email message with To Address mappings
        casesToInsert.add(new Case(Status='New', Origin='Twitter - Addison Lee', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//2 24 hr entitlement from generic account
        casesToInsert.add(new Case(Status='New',Type='Incident', Origin='Origin', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//3 24 hr entitlement from generic account
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//4 for email message with To Address mappings
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//1 for email message with To Address mappings
        casesToInsert.add(new Case(AccountId=accountsList[5].id,Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//1 for email message with To Address mappings
        casesToInsert.add(new Case(AccountId=childAccountsList[3].id,Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        casesToInsert.add(new Case(AccountId=childAccountsList[4].id,Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        insert casesToInsert;
            
        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[1].Id, ToAddress='example0@testclass.com',Incoming = true));
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[4].Id, ToAddress='example1@testclass.com',Incoming = true));
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[3].Id, ToAddress='example0@testclass.com',Incoming = true));

        insert emailMessages;
        Test.StartTest();
        Map<id,Case> caseMap= new Map<Id,Case>([Select Id,origin,ParentId,AccountId,EntitlementId from Case]);

        /*system.assertEquals(caseMap.get(casesToInsert[0].Id).EntitlementId,entitlementList[2].Id);
        system.assertEquals(caseMap.get(casesToInsert[1].Id).AccountId,accountsList[3].Id); // case will not be associated with the Account - UPDATE Case will be associated as per requirements change
        system.assertEquals(caseMap.get(casesToInsert[2].Id).EntitlementId,entitlementList[0].Id);// twitter cases
        system.assertEquals(caseMap.get(casesToInsert[3].Id).EntitlementId,entitlementList[0].Id);// incident cases
        system.assertEquals(caseMap.get(casesToInsert[4].Id).EntitlementId,entitlementList[1].Id);// 48 hour entitlement
        system.assertEquals(caseMap.get(casesToInsert[5].Id).EntitlementId,entitlementList[2].Id);// 72 hour entitlement
        system.assertEquals(caseMap.get(casesToInsert[6].Id).EntitlementId,entitlementList[5].Id);
        system.assertEquals(caseMap.get(casesToInsert[7].Id).EntitlementId,null);
        system.assertEquals(caseMap.get(casesToInsert[8].Id).EntitlementId,entitlementList[5].Id);*/
        Test.StopTest();

    }

    Static testMethod void validatePopulateMilestoneCompletionDate() {
        Id caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Reactive Cases').getRecordTypeId();
        AddLee_Trigger_Test_Utils.insertCustomSettings();

        List<Account> accountsList = new List<Account>();
        accountsList.add(new Account(Name = 'example0@testclass.com Account',exsistInShamrock__c=true,Account_Number__c='1', Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting'));
        insert accountsList;

        List<Contact> contactsList = new List<Contact>();
        contactsList.add(new Contact(LastName = 'Test', AccountId = accountsList[0].Id,  Contact_Type__c = 'Main Contact',Email='example@testexample.com'));
        insert contactsList;

        SLAProcess slaObj=[SELECT Id,IsActive,Name,NameNorm FROM SlaProcess WHERE NameNorm like '%24 hour support%' AND IsVersionDefault= true AND IsActive= true limit 1];
        List<Entitlement> entitlementList= new List<Entitlement>();
        entitlementList.add(new Entitlement(SlaProcessId=slaObj.id,Name='Account',AccountId=accountsList[0].Id,StartDate=system.Today().addDays(-1)));
        insert entitlementList;

        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email',ContactId=contactsList[0].Id, Subject='Test Subject',RecordTypeId=caseRecordTypeId,AccountId=accountsList[0].Id ));
        insert casesToInsert;

        casesToInsert[0].Status='Closed';
        casesToInsert[0].Type = 'Admin';
        casesToInsert[0].Case_Type__c = 'Admin Fee Discount';
        update casesToInsert;
        List<CaseMilestone> CaseMilestones = [Select Id,CaseId,CompletionDate from CaseMilestone where CaseId =: casesToInsert[0].Id];
        for(CaseMilestone cm : CaseMilestones){
            system.assertNotEquals(cm.CompletionDate,null);
        }
    }
}