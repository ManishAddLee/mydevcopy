@isTest
public with sharing class AddLee_ContactHelper_Test {
	
	@isTest(seeAllData=false)
	public static void Contact_ContactInsert_IsContactInserted(){
		Integer expectedContacts = 10;
		String shm1 = 'Manish Thakur';
		String shm2 = 'Manish Singh Thakur';
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		
		List<Contact> contactsToInsert = AddLee_Trigger_Test_Utils.createContacts(expectedContacts);
		List<Contact> contactsToInsertSet1 = AddLee_Trigger_Test_Utils.createAllContacts(1, shm1);
		List<Contact> contactsToInsertSet2 = AddLee_Trigger_Test_Utils.createAllContacts(1, shm2);
		
		List<Contact> allContactsToInsert = new List<Contact>(contactsToInsert);
		allContactsToInsert.addAll(contactsToInsertSet1);
		allContactsToInsert.addAll(contactsToInsertSet2);
		
		Test.startTest();
			insert allContactsToInsert;
		Test.stopTest();
		
		List<Contact> contactToTest1 = [Select FirstName, Lastname, SHM_contactName__c, HasOptedOutOfEmail, Optout_All__c, SMS_Opt_Out__c, Contact_Type__c,
		                                       Add_Lib_Lifestyle_Content_Email__c, Add_Lib_Lifestyle_Content_Post__c
		                                From Contact Where SHM_contactName__c =: shm1];
		List<Contact> contactToTest2 = [Select FirstName, Lastname, SHM_contactName__c, HasOptedOutOfEmail, Optout_All__c, SMS_Opt_Out__c, Contact_Type__c From Contact Where SHM_contactName__c =: shm2];
		System.assertEquals(expectedContacts,[SELECT count() FROM Contact WHERE Id in :contactsToInsert]);
		
		System.assertEquals(contactToTest1[0].FirstName,'Manish');
		System.assertEquals(contactToTest1[0].Lastname,'Thakur');
		System.assertEquals(contactToTest1[0].HasOptedOutOfEmail, true);
		System.assertEquals(contactToTest1[0].Optout_All__c, true);
		System.assertEquals(contactToTest1[0].SMS_Opt_Out__c, true);
		System.assertEquals(contactToTest1[0].Contact_Type__c, 'Main Contact');
		System.assertEquals(contactToTest1[0].Add_Lib_Lifestyle_Content_Email__c, false);
		// Workflow rule:Contact OptOut All Rule sets the value back to false
		System.assertEquals(contactToTest1[0].Add_Lib_Lifestyle_Content_Post__c, false); 
		
		System.assertEquals(contactToTest2[0].FirstName,'Manish');
		System.assertEquals(contactToTest2[0].Lastname,'Singh Thakur');
		System.assertEquals(contactToTest2[0].HasOptedOutOfEmail, true);
		System.assertEquals(contactToTest2[0].Optout_All__c, true);
		System.assertEquals(contactToTest2[0].SMS_Opt_Out__c, true);
	}
	
	@isTest(seeAllData=false)
	public static void Contact_IsContactUpdated(){
		String shm1 = 'Manish Thakur';
		String shm2 = 'Manish Singh Thakur';
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		
		List<Contact> contactsToInsertSet1 = AddLee_Trigger_Test_Utils.createAllContacts(1, shm1);
		List<Contact> contactsToInsertSet2 = AddLee_Trigger_Test_Utils.createAllContacts(1, shm2);
		
		List<Contact> allContactsToInsert = new List<Contact>();
		allContactsToInsert.addAll(contactsToInsertSet1);
		allContactsToInsert.addAll(contactsToInsertSet2);
		insert allContactsToInsert;
		
		list<Contact> contactToUpdate = [Select Id From Contact];
		Test.startTest();
		    // Reset static variables
		    AddLee_checkRecursive.isAfterInsertUpdate = true;
		    AddLee_checkRecursive.firstRunContactIsBefore = true;
		    AddLee_checkRecursive.firstRunContactIsAfter = true;
		    update contactToUpdate;
		Test.stopTest();
		
		
		
		List<Contact> contactToTest1 = [Select FirstName, Lastname, SHM_contactName__c, HasOptedOutOfEmail, Optout_All__c, SMS_Opt_Out__c, Contact_Type__c,
		                                       Add_Lib_Lifestyle_Content_Email__c, Add_Lib_Lifestyle_Content_Post__c
		                                From Contact Where SHM_contactName__c =: shm1];
		List<Contact> contactToTest2 = [Select FirstName, Lastname, SHM_contactName__c, HasOptedOutOfEmail, Optout_All__c, SMS_Opt_Out__c, Contact_Type__c From Contact Where SHM_contactName__c =: shm2];
		
		System.assertEquals(contactToTest1[0].FirstName,'Manish');
		System.assertEquals(contactToTest1[0].Lastname,'Thakur');
		System.assertEquals(contactToTest1[0].HasOptedOutOfEmail, true);
		System.assertEquals(contactToTest1[0].Optout_All__c, true);
		System.assertEquals(contactToTest1[0].SMS_Opt_Out__c, true);
		System.assertEquals(contactToTest1[0].Contact_Type__c, 'Main Contact');
		System.assertEquals(contactToTest1[0].Add_Lib_Lifestyle_Content_Email__c, false);
		// Workflow rule:Contact OptOut All Rule sets the value back to false
		System.assertEquals(contactToTest1[0].Add_Lib_Lifestyle_Content_Post__c, false); 
		
		System.assertEquals(contactToTest2[0].FirstName,'Manish');
		System.assertEquals(contactToTest2[0].Lastname,'Singh Thakur');
		System.assertEquals(contactToTest2[0].HasOptedOutOfEmail, true);
		System.assertEquals(contactToTest2[0].Optout_All__c, true);
		System.assertEquals(contactToTest2[0].SMS_Opt_Out__c, true);
	}
}