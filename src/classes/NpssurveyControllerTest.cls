@istest(seealldata = false)
public class NpssurveyControllerTest {
    
    public static Log_Integration__c CreateLogIntegrationRec() {
        Log_Integration__c LogIntegration = new Log_Integration__c();
        LogIntegration.name = 'shamrock_setting';
        LogIntegration.CurrentSessionId__c = 'TEST';
        LogIntegration.Enable_Bulk_Processing__c = false;
        LogIntegration.enableUpdates__c = false;
        LogIntegration.EndPoint__c = 'ttps://wsplatform.addisonlee.com/customer-management-ws/CustomerManagementWebService1.0';
        LogIntegration.Password__c = 'salesforce';
        LogIntegration.Username__c = 'salesforce';
        insert LogIntegration;
        return LogIntegration;
    }
    
    public static testmethod void NpsTest() {
        Log_Integration__c logIntegration = CreateLogIntegrationRec();
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account acc = new account(name = 'abc acc');
        insert acc;
        contact con = new contact(lastname = 'test contact',accountid = acc.id);
        insert con;
        
        Test.startTest();
        NPS__c npsRec = new NPS__c(External_Id__c = 'TestNP00001',Contact__c = con.id, Account__c = acc.Id);
        insert npsRec;
        ApexPages.currentpage().getParameters().put('selection', '4');
        ApexPages.currentpage().getParameters().put('pid', 'TestNP00001');
        NpssurveyController Np = new NpssurveyController();
        Np.updImage();
        Np.updcategory();
        Np.Submit();
        
        ApexPages.currentpage().getParameters().put('selection', '2');
        Np.Submit();
        
        ApexPages.currentpage().getParameters().put('selection', '3');
        Np.Submit();
        
        ApexPages.currentpage().getParameters().put('selection', '1');
        Np.Submit();
        Np.Redirect();
        
        
        NPS__c updnps=new NPS__c(Id=npsRec.Id);
        updnps.External_Id__c ='';
        update updnps;
        
        Np.Submit();
        Test.stopTest();
        
    }
    public static testmethod void NpsTest1() {
        
        ApexPages.currentpage().getParameters().put('selection', '2');
        ApexPages.currentpage().getParameters().put('id', 'TestNP00001');
        NpssurveyController Np = new NpssurveyController();
        Np.Submit();
        
    }
    public static testmethod void NpsTest2() {
        
        ApexPages.currentpage().getParameters().put('selection', '3');
        ApexPages.currentpage().getParameters().put('id', 'TestNP00001');
        NpssurveyController Np = new NpssurveyController();
        Np.Submit();
        
    }
    
    /*public static testmethod void NpsTest3() {

ApexPages.currentpage().getParameters().put('selection', '6');
ApexPages.currentpage().getParameters().put('id', '');
NpssurveyController Np = new NpssurveyController();
Np.updImage();
Np.updcategory();
Np.Submit();
Np.Redirect();

}*/
}