public with sharing class Utility {
    
    public static void processErrorLog(String errorCode,String errorDescription,String parentCaseid,String id,String numberval){
        AdleeServiceLogger__c logger = new AdleeServiceLogger__c();
        logger.ErrorCode__c = errorCode;
        logger.ErrorDescription__c = errorDescription;
        logger.ParentCase__c = parentCaseid;
        logger.id__c = id;
        logger.Number__c = numberval;
        Database.insert(logger);
    }
}