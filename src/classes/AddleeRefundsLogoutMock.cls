@isTest
global class AddleeRefundsLogoutMock implements WebserviceMock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.logoutResponse respElement =  new Addlee_refunds.logoutResponse();
          
       response.put('response_x', respElement); 
   }
}