/* test class for Addlee_EmailSMSOptoutHandler
 ** developed by Veeran
 *** Date 08/08/2015
 */

@isTest
public class Addlee_EmailSMSOptoutHandler_Test
{
    public static testmethod void testmeth()
    {
       
        Addlee_Trigger_Test_Utils.insertCustomSettings();
        RecordType r = new RecordType();
        r=[select Id,SobjectType,Name from RecordType where Name='Business Account' and sobjectType='Account' limit 1];
        Account acc = new Account();
        acc.Name='Test';
        acc.RecordtypeId=r.id;
        insert acc;
        Contact c = new contact();
        c.LastName='Test';
        c.AccountId=acc.Id;
        c.Optout_All__c=true;
        insert c;
        c.HasOptedOutOfEmail = false;
        update c;
        c.HasOptedOutOfEmail = true;
        update c;
        c.SMS_Opt_Out__c = false;
        update c;
        c.SMS_Opt_Out__c = true;
        update c;
        c.Add_Lib_Lifestyle_Content_Email__c = false;
        update c;
        c.Add_Lib_Lifestyle_Content_Email__c = true;
        update c;
        c.Add_Lib_Lifestyle_Content_SMS__c = false;
        update c;
        c.Add_Lib_Lifestyle_Content_SMS__c = true;
        update c;
        c.Optout_All__c = false;
        update c; 
        c.Optout_All__c = true;
        update c;
    }
}