@istest(seealldata = false) 
public class NPSUpdaterTest {
    public static Log_Integration__c CreateLogIntegrationRec() {
        Log_Integration__c LogIntegration = new Log_Integration__c();
        LogIntegration.name = 'shamrock_setting';
        LogIntegration.CurrentSessionId__c = 'TEST';
        LogIntegration.Enable_Bulk_Processing__c = false;
        LogIntegration.enableUpdates__c = false;
        LogIntegration.EndPoint__c = 'ttps://wsplatform.addisonlee.com/customer-management-ws/CustomerManagementWebService1.0';
        LogIntegration.Password__c = 'salesforce';
        LogIntegration.Username__c = 'salesforce';
        insert LogIntegration;
        return LogIntegration;
    }
    public static testmethod void TestNPS() {
        Log_Integration__c LogIntegration = NpssurveyControllerTest.CreateLogIntegrationRec();
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account acc = AddLee_Trigger_Test_Utils.createAccounts(1).get(0);
        
        Test.startTest();
        
        insert acc;
        contact con = new contact(lastname = 'test contact',accountid = acc.id);
        insert con;
        List<Booking_Summary__c> testBookingSummary = AddLee_Trigger_Test_Utils.createBookingSummaryTest(acc.Id, 1);
        testBookingSummary[0].Pick_Up_time__c =DateTime.now();
        insert testBookingSummary;

        
        NPS__c npsRec = new NPS__c(External_Id__c = 'TestNP00001',Contact__c = con.id, Account__c = acc.id,Score__c = 'Green',Comments__c ='Test Comments');
        insert npsRec;
        
        Interaction__c inte=new Interaction__c();
        inte.Booking_Summary__c = testBookingSummary[0].Id;
        inte.NPS__c = npsRec.Id;
        insert inte;
        
        
        npsRec.Score__c = 'Amber';
        npsRec.Submit__c=true;
        update npsRec;
        
        Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        
        Test.stopTest();
    }
}