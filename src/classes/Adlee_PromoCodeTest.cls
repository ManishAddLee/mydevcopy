@isTest
public class Adlee_PromoCodeTest
{
    public static TestMethod void createPromocode()
    {
    
        PromoCodeValue__c pcv= new PromoCodeValue__c();
        pcv.Name ='Promo code Values';
        pcv.Email__c ='test@tes.com';
        pcv.No_of_Promo_Codes__c ='1';
        insert pcv;
                
        Test.startTest();
        
        Promo_Code__c pc=new Promo_Code__c();
        pc.Allocated__c =false;
        pc.Allocated_Date_Time__c = System.now();
        pc.Authorised_By__c =UserInfo.getUserId();
        pc.Code__c ='ABC';
        pc.No_of_Codes_Left__c =10;
        
        insert pc;
        
        
        pc.Allocated__c =true;
        update pc;
            
        Test.stopTest();
    }
}