Public class UpdateCaseHandler {
    public static void updatecase(list<task> Tasklist)
    {
        set<id> CaseIdSet = new set<id>();
        list<case> CaseList = new list<Case>();
        for(task t: Tasklist) {
            if((t.subject == 'Call' || t.subject == 'Email:Thanks for your feedback') && t.status == 'Completed' && t.whatid != null && t.whatid.getSObjectType().getDescribe().getName() == 'Case') {
                CaseIdSet.add(t.whatid);
            }
        }
        for(case c : [select id,X1st_Reponse__c from case where id in : CaseIdSet]) {
            c.X1st_Reponse__c = true;
            caselist.add(c);
        }
        if(caseList.size() > 0)
            update CaseList;
    }
    public static void UpdateCaseFromEmailMeassage(list<emailMessage> EmailMsgList) {
        set<id> CaseIdSet = new set<id>();
        list<case> CaseList = new list<Case>();
        for(EmailMessage e : EmailMsgList) {
            if(e.parentid != null && e.parentid.getSObjectType().getDescribe().getName() == 'Case' && e.FromAddress != 'noreply@addisonlee.com' ) {
                CaseIdSet.add(e.parentid);
            }
        }
        for(case c : [select id,X1st_Reponse__c from case where id in : CaseIdSet]) {
                c.X1st_Reponse__c = true;
                caselist.add(c);
            }
            if(caseList.size() > 0)
                update CaseList;
     }
 
}