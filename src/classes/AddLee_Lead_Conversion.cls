public without sharing class AddLee_Lead_Conversion {
	List<JNC_CompetitorAccount__c> competitorAccountsToInsert = new List<JNC_CompetitorAccount__c>();
	Set<Id> convertedLeads = new Set<Id>();
	public void copyCompetitorsToAccountOnConversion(Map<Id, Lead> newObjectsMap){
		for(Lead eachLead: newObjectsMap.values()){
			if(eachLead.IsConverted){
				convertedLeads.add(eachLead.Id);
			}
		}
		if(!convertedLeads.isEmpty()){
			for(JNC_CompetitorLead__c eachLeadCompetitor : [SELECT Id, Lead__c, Lead__r.isConverted, Competitor__c FROM JNC_CompetitorLead__c WHERE Lead__c in :convertedLeads]){
				if(eachLeadCompetitor.Lead__r.isConverted){
					JNC_CompetitorAccount__c competitorAccount = new JNC_CompetitorAccount__c(Account__c = newObjectsMap.get(eachLeadCompetitor.Lead__c).ConvertedAccountId,Competitor__c = eachLeadCompetitor.Competitor__c);
					competitorAccountsToInsert.add(competitorAccount);
				}
			}
			if(!competitorAccountsToInsert.isEmpty()){
				insert competitorAccountsToInsert;	
			}
		}
	}
	@future(callout=true)
	public static void convertLeadToCurrentAccount(Set<Id> newObjectsId){
		List<Database.LeadConvert> leadConvertList = new List<Database.LeadConvert>();
		System.debug('Gunish');
		for(Lead eachLead : [SELECT Id, isConverted, Convert_To_Account__c, RAG_Status__c, Bank_Details_Captured__c FROM Lead WHERE Id in:newObjectsId]){
			System.DEBUG(' Looping through Leads -- converted ? ' +  eachLead.isConverted + ' ' + eachLead.RAG_Status__c);
			//System.DEBUG(' Looping through Leads -- oldValue ? ' +  oldObjectsMap.get(eachLead.Id).Bank_Details_Captured__c + ' ' + eachLead);
			if(!eachLead.isConverted){
				if(eachLead.RAG_Status__c != null && eachLead.RAG_Status__c.equalsIgnoreCase('green') 
					&& eachLead.Bank_Details_Captured__c && eachLead.Convert_To_Account__c){
					System.DEBUG('In Here -- ');
					Database.LeadConvert convertLead = new Database.LeadConvert();
					convertLead.setLeadId(eachLead.Id);
					convertLead.setConvertedStatus('Qualified');
					convertLead.setDoNotCreateOpportunity(true);
					leadConvertList.add(convertLead);
				}
			}
		}
		Database.LeadConvertResult[] LeaConvResults = Database.convertLead(leadConvertList,false);
		System.DEBUG('Lead Convert Results : ' + LeaConvResults);

	}
	
	@future(callout=true)
	public static void convertLeadonSchedule(Set<Id> newObjectsId){
		Map<Id,String> tAndCAcceptedMap = new Map<Id,String>();
		for(AddLee_Terms_Conditions__c eachTC : [SELECT Internal_Id__c, Response__c FROM AddLee_Terms_Conditions__c WHERE Internal_Id__c in:newObjectsId
													And Response__c = 'Accepted']){
			tAndCAcceptedMap.put(eachTC.Internal_Id__c, eachTC.Response__c);
		}
		List<Database.LeadConvert> leadConvertList = new List<Database.LeadConvert>();
		for(Lead eachLead : [SELECT Id, createdById, isConverted, Convert_To_Account__c, RAG_Status__c, Bank_Details_Captured__c FROM Lead WHERE Id in:newObjectsId]){
			System.DEBUG(' Looping through Leads -- converted ? ' +  eachLead.isConverted + ' ' + eachLead.RAG_Status__c);
			//System.DEBUG(' Looping through Leads -- oldValue ? ' +  oldObjectsMap.get(eachLead.Id).Bank_Details_Captured__c + ' ' + eachLead);
			if(!eachLead.isConverted){
				System.DEBUG('Its not by a Web User');
				if(eachLead.RAG_Status__c != null && eachLead.RAG_Status__c.equalsIgnoreCase('green') 
					&& eachLead.Bank_Details_Captured__c && eachLead.Convert_To_Account__c){
					System.DEBUG('Qualified');
					system.debug(tAndCAcceptedMap.get(eachLead.Id));
					if(tAndCAcceptedMap.get(eachLead.Id) != null && tAndCAcceptedMap.get(eachLead.Id) == 'Accepted'){
						Database.LeadConvert convertLead = new Database.LeadConvert();
						convertLead.setLeadId(eachLead.Id);
						convertLead.setConvertedStatus('Qualified');
						convertLead.setDoNotCreateOpportunity(true);
						leadConvertList.add(convertLead);
					}
				}
			}
		}
		Database.LeadConvertResult[] LeaConvResults = Database.convertLead(leadConvertList,false);
		System.DEBUG('Lead Convert Results : ' + LeaConvResults);
	}
}