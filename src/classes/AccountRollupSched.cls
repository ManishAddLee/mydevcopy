//*********************************************************************************** 
//
//  AccountRollupBatch
//
//  Desciption:
//  This class was created by InSitu Software for AddisonLee. It implements the 
//  Schedulable interface and is provided so that the Account Rollup Calc processing 
//  can be scheduled to run on a regular basis. 
//	FOR EXCLUSIVE USE OF ADDISON LEE ONLY
//
//  History:
//  InSitu  02/16/20165  Original version.
// ***********************************************************************************

global with sharing class AccountRollupSched implements Schedulable
{
    global void execute(SchedulableContext SC) 
    {
        // Use the static helper method to queue up the batch job.
        // Process all UP accounts in batches of 20 records, perform AssetRollup.
        Id idJob = AccountRollupBatch.runAccountRollupBatch(0, 20, AccountRollupBatch.RUTYPE_RELATEDOBJS);
        
    }
}