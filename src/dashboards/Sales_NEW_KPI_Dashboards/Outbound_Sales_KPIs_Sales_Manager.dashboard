<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Tyrone Sullivan</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>James Martin</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Anna-Maria Rains</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Terry Gacias</values>
        </dashboardFilterOptions>
        <name>Account Owner</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>30000.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Current Month Revenue for Accounts Opened/Reactivated in Last 6 Mths</footer>
            <header>Monthly Revenue Target</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Monthly_Revenue_Target</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target: 20K - 30K</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>100.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Opened/Reactivated in Last 6 Months with Use This Month</footer>
            <header>Opened/Reactivated Last 6 Months - Used This Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Using_This_Month_Cohort</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Target: 100</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked100</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Account.Date_changed_to_Current__c</groupingColumn>
            <groupingColumn>Account.Date_of_Last_Booking__c</groupingColumn>
            <header>Account Retention From Creation</header>
            <legendPosition>Right</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Sales_Account_Retention</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>BarStacked100</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Opened/Reactivated This Month by Payment Type</footer>
            <groupingColumn>USERS.NAME</groupingColumn>
            <groupingColumn>Account.SHM_invoiceClearanceType__c</groupingColumn>
            <header>Direct Debit Uptake</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Direct_Debit_Uptake</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target 70%</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>50.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Opened This Month that have used This Month</footer>
            <header>New Business - Opened This Month - Used This Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Use_This_Month_TM</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target 50</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>ACCOWNER</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Open Activities - Current &amp; Previous Month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Open_Activities</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>50.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>% of Accounts Opened/Reactivated 6 mths ago that have used This Mth</footer>
            <groupingColumn>USERS.NAME</groupingColumn>
            <header>Retention Rate</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Retention_Rate</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target: 50%</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>20.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Reactivated this Month that have used This Month</footer>
            <header>Reactivated Accounts - Used This Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_Reactivated_and_Used_This_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target: 20</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>4.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>ACCOWNER</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Outbound Face to Face Meetings this Month by Week</footer>
            <header>Outbound Face to Face Meetings this Month by Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Outbound_F2F_Meetings_This_Month_by_Week2</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target: 4 Meetings per Week</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>philip.dickinson@addisonlee.com</runningUser>
    <textColor>#000000</textColor>
    <title>Outbound Sales KPIs (Sales Manager)</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
