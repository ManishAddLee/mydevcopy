trigger AddLee_CaseComment on CaseComment (before insert, before update, before delete, 
                                after insert, after update, after delete, after undelete) {

    Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    /* Get singleton handler's instance */
    AddLee_TriggerCaseCommentHandler handler = new AddLee_TriggerCaseCommentHandler();

    /* Before Insert 
    if (Trigger.isInsert && Trigger.isBefore) {
        handler.onBeforeInsert(Trigger.new);
    }
    */
    /* After Insert */
    if (Trigger.isAfter && Trigger.isInsert) {
        handler.onAfterInsert(Trigger.new, Trigger.newMap);
    }
   
    /* Before Update 
    else if (Trigger.isUpdate && Trigger.isBefore) {
        handler.onBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }
    */
    /* After Update 
    else if (Trigger.isUpdate && Trigger.isAfter) {
        handler.onAfterUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }
    */
    /* Before Delete
    else if (Trigger.isDelete && Trigger.isBefore) {
        handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    */
    /* After Delete
    else if (Trigger.isDelete && Trigger.isAfter) {
        handler.onAfterDelete(Trigger.old, Trigger.oldMap);
    }
    */    
    /* After Undelete
    else if (Trigger.isUnDelete) {
        handler.onAfterUndelete(Trigger.new, Trigger.newMap);
    }
    */
}